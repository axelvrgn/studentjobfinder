package com.studentjobfinder.studentjobfinder.dto.in;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

@Getter
@Setter
public class CompanyBean {
    private String companyId;
    private String accountId;
    @NotBlank
    private String name;
    private String description;
    @Nullable
    private String sectorActivity;
    @Nullable
    private String website;
}
