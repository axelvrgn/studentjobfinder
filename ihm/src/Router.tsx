import { Outlet, RouterProvider, createBrowserRouter } from "react-router-dom";
import { Layout } from "./layouts/Layout";
import { HomeView } from "./pages/Home/HomeView";
import Error from "./pages/Error/Error";
import LoginView from "./pages/Authentication/Login/LoginView";
import RegisterView from "./pages/Authentication/Register/RegisterView";
import CompaniesView from "./pages/Company/CompaniesView";
import JobView from "./pages/Job/JobView";

function Router() {
  const router = createBrowserRouter([
    {
      element: (
        <Layout>
          <Outlet />
        </Layout>
      ),
      errorElement: <Error />,
      children: [
        {
          path: "/",
          element: <HomeView />,
        },
        {
          path: "/companies",
          element: <CompaniesView />,
        },
        {
          path: "/job",
          element: <JobView />,
        },
        {
          path: "/login",
          element: <LoginView />,
        },
        {
          path: "/register",
          element: <RegisterView />,
        },
      ],
    },
  ]);

  return <RouterProvider router={router} />;
}

export default Router;
