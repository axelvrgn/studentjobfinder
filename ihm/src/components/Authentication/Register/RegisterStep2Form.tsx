import { useState } from "react";
import {
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
type FormRegisterStep2Props = {
  saveAccountCredentials: (email: string, password: string) => void;
};

interface IFormRegisterStep2 {
  email: string;
  password: string;
  passwordVerif: string;
}

function FormRegisterStep2({ saveAccountCredentials }: FormRegisterStep2Props) {
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const {
    register,
    watch,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<IFormRegisterStep2>({
    mode: "onBlur",
  });

  const saveCredentials = (formRegisterStep2Values: IFormRegisterStep2) => {
    saveAccountCredentials(
      formRegisterStep2Values.email,
      formRegisterStep2Values.password
    );
  };

  const handleClickShowPassword = () => setShowPassword(!showPassword);

  return (
    <form className="flex flex-col" onSubmit={handleSubmit(saveCredentials)}>
      <FormControl mb={3} isInvalid={!!errors.email} isRequired>
        <Input
          id="email"
          {...register("email", { required: "Champ requis" })}
          type="mail"
          placeholder="Adresse e-mail"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.email && errors.email.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.password} isRequired>
        <InputGroup size="md">
          <Input
            {...register("password", { required: "Champ requis" })}
            type={showPassword ? "text" : "password"}
            placeholder="Mot de passe"
            focusBorderColor="purple.500"
            pr="5rem"
          />
          <InputRightElement width="4.5rem" mr="0.2rem">
            <Button h="1.75rem" size="sm" onClick={handleClickShowPassword}>
              {showPassword ? "Masquer" : "Afficher"}
            </Button>
          </InputRightElement>
        </InputGroup>
        <FormHelperText>Nécessite au moins 8 caractères</FormHelperText>
        <FormErrorMessage>
          {errors.password && errors.password.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.passwordVerif} isRequired>
        <Input
          id="passwordVerif"
          {...register("passwordVerif", {
            required: "Champ requis",
            validate: (val: string) => {
              if (watch("password") != val) {
                return "Les mots de passe doivent être identique";
              }
            },
          })}
          type="password"
          placeholder="Confirmation du mot de passe"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.passwordVerif && errors.passwordVerif.message}
        </FormErrorMessage>
      </FormControl>
      <Button type="submit" colorScheme="purple" mt={3} isDisabled={!isValid}>
        Suivant
      </Button>
    </form>
  );
}

export default FormRegisterStep2;
