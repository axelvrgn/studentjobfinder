import { Navbar } from "../components/navigation/Navbar";

type LayoutProps = {
  children?: JSX.Element | JSX.Element[];
};

export function Layout({ children }: LayoutProps) {
  return (
    <>
      <header>
        <Navbar />
      </header>
      <main>{children}</main>
      <footer></footer>
    </>
  );
}
