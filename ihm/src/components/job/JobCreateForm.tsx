import {
  Button,
  Flex,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  Select,
  Textarea,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { JOB_TYPES, MESSAGE_REQUIRED } from "../../data/constants";

interface IJobCreateForm {
  title: string;
  description: string;
  startDate: string;
  salary: string;
  type: string;
  advantages: string;
  address: string;
  city: string;
  postalCode: string;
}

function JobCreateForm() {
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<IJobCreateForm>({
    mode: "onBlur",
  });

  const createJob = () => {
    return null;
  };

  return (
    <form className="flex flex-col w-full" onSubmit={handleSubmit(createJob)}>
      <FormControl mb={3} isInvalid={!!errors.title} isRequired>
        <Input
          {...register("title", { required: MESSAGE_REQUIRED })}
          type="text"
          placeholder="Titre"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.title && errors.title.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl mb={3} isInvalid={!!errors.description}>
        <Textarea
          {...register("description")}
          placeholder="Description"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.description && errors.description.message}
        </FormErrorMessage>
      </FormControl>
      <div>
        <Flex alignItems={"flex-end"} gap={4}>
          <FormControl mb={3} isInvalid={!!errors.type} isRequired>
            <Select
              {...register("type", { required: MESSAGE_REQUIRED })}
              placeholder="Type de contrat"
              focusBorderColor="purple.500"
            >
              {JOB_TYPES.map((type) => (
                <option key={type} value={type}>
                  {type}
                </option>
              ))}
            </Select>
            <FormErrorMessage>
              {errors.type && errors.type.message}
            </FormErrorMessage>
          </FormControl>
          <FormControl mb={3} isInvalid={!!errors.startDate} isRequired>
            <FormLabel htmlFor="startDate">Début du contrat</FormLabel>
            <Input
              id="startDate"
              {...register("startDate", { required: MESSAGE_REQUIRED })}
              type="date"
              placeholder="Début du contrat"
              focusBorderColor="purple.500"
            />
            <FormErrorMessage>
              {errors.startDate && errors.startDate.message}
            </FormErrorMessage>
          </FormControl>
        </Flex>
      </div>
      <FormControl mb={3} isInvalid={!!errors.address} isRequired>
        <Input
          {...register("address", { required: MESSAGE_REQUIRED })}
          type="text"
          placeholder="Adresse"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.address && errors.address.message}
        </FormErrorMessage>
      </FormControl>
      <Flex gap={4}>
        <FormControl mb={3} isInvalid={!!errors.city} isRequired>
          <Input
            {...register("city", { required: MESSAGE_REQUIRED })}
            type="text"
            placeholder="Ville"
            focusBorderColor="purple.500"
          />
          <FormErrorMessage>
            {errors.city && errors.city.message}
          </FormErrorMessage>
        </FormControl>
        <FormControl mb={3} isInvalid={!!errors.postalCode} isRequired>
          <Input
            {...register("postalCode", { required: MESSAGE_REQUIRED })}
            type="number"
            placeholder="Code postal"
            focusBorderColor="purple.500"
          />
          <FormErrorMessage>
            {errors.postalCode && errors.postalCode.message}
          </FormErrorMessage>
        </FormControl>
      </Flex>

      <Button type="submit" isDisabled={!isValid} colorScheme={"purple"}>
        Enregistrer
      </Button>
    </form>
  );
}

export default JobCreateForm;
