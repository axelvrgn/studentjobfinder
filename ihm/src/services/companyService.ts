import Axios from "./callerService";
import {
  CompanyBean,
  PageableCompanyBean,
} from "./interfaces-generated/STUDENTJOBFINDER-INTERFACES";
type addCompanyQueryParams = {
  accountId: string;
  name: string;
};

type paginatedQueryParams = {
  page: number;
  size: number;
};

const addCompany = async ({ accountId, name }: addCompanyQueryParams) => {
  return Axios.post<string>("/company/addCompany", {
    accountId,
    name,
  });
};

const getCompanyByAccountId = async (accountId: string) => {
  return Axios.get<CompanyBean>("/company/getCompanyByAccountId", {
    params: { accountId },
  });
};

const getCompaniesPaginated = async ({ page, size }: paginatedQueryParams) => {
  return Axios.get<PageableCompanyBean>("/company/getCompaniesPaginated", {
    params: { page, size },
  });
};

const getCompaniesByStatusVerifiedPaginated = async ({
  page,
  size,
}: paginatedQueryParams) => {
  return Axios.get<CompanyBean[]>("/company/getCompaniesPaginated", {
    params: { page, size },
  });
};

const updateCompany = async (company: CompanyBean) => {
  return Axios.patch<string>("/company/getCompaniesPaginated", company);
};

export const companyService = {
  addCompany,
  getCompanyByAccountId,
  getCompaniesPaginated,
  getCompaniesByStatusVerifiedPaginated,
  updateCompany,
};
