package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import com.studentjobfinder.studentjobfinder.Model.AccountStatus;
import com.studentjobfinder.studentjobfinder.services.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/account")
public class AccountController {

    @Autowired
    IAccountService accountService;

    @PatchMapping("/updateAccountStatusToVerified")
    public ResponseEntity<String> updateAccountStatusToVerified(@RequestParam String accountId) {
        accountService.updateAccountStatut(accountId, AccountStatus.VERIFIED);
        return ResponseEntity.ok("Statut du compte mis à jour avec succès");
    }

    @PatchMapping("/updateAccountStatusToBanned")
    public ResponseEntity<String> updateAccountStatusToBanned(@RequestParam String accountId) {
        accountService.updateAccountStatut(accountId, AccountStatus.BANNED);
        return ResponseEntity.ok("Statut du compte mis à jour avec succès");
    }

    @PatchMapping("/updateAccountStatusToBlocked")
    public ResponseEntity<String> updateAccountStatusToBlocked(@RequestParam String accountId) {
        accountService.updateAccountStatut(accountId, AccountStatus.BLOCKED);
        return ResponseEntity.ok("Statut du compte mis à jour avec succès");
    }

    @PatchMapping("/updateAccountTypeToAdmin")
    public ResponseEntity<String> updateAccountTypeToAdmin(@RequestParam String accountId) {
        accountService.updateAccountRole(accountId, AccountRole.ADMIN);
        return ResponseEntity.ok("Type du compte mis à jour avec succès");
    }

}
