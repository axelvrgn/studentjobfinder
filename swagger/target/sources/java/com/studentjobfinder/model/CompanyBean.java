package com.studentjobfinder.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.studentjobfinder.model.AccountBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CompanyBean
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-23T17:02:46.256+01:00")

public class CompanyBean   {
  @JsonProperty("companyId")
  private String companyId = null;

  @JsonProperty("account")
  private AccountBean account = null;

  @JsonProperty("companyName")
  private String companyName = null;

  @JsonProperty("companyDescription")
  private String companyDescription = null;

  @JsonProperty("companySectorActivity")
  private String companySectorActivity = null;

  @JsonProperty("companyWebSite")
  private String companyWebSite = null;

  public CompanyBean companyId(String companyId) {
    this.companyId = companyId;
    return this;
  }

  /**
   * Get companyId
   * @return companyId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public CompanyBean account(AccountBean account) {
    this.account = account;
    return this;
  }

  /**
   * Get account
   * @return account
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public AccountBean getAccount() {
    return account;
  }

  public void setAccount(AccountBean account) {
    this.account = account;
  }

  public CompanyBean companyName(String companyName) {
    this.companyName = companyName;
    return this;
  }

  /**
   * Get companyName
   * @return companyName
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public CompanyBean companyDescription(String companyDescription) {
    this.companyDescription = companyDescription;
    return this;
  }

  /**
   * Get companyDescription
   * @return companyDescription
  **/
  @ApiModelProperty(value = "")


  public String getCompanyDescription() {
    return companyDescription;
  }

  public void setCompanyDescription(String companyDescription) {
    this.companyDescription = companyDescription;
  }

  public CompanyBean companySectorActivity(String companySectorActivity) {
    this.companySectorActivity = companySectorActivity;
    return this;
  }

  /**
   * Get companySectorActivity
   * @return companySectorActivity
  **/
  @ApiModelProperty(value = "")


  public String getCompanySectorActivity() {
    return companySectorActivity;
  }

  public void setCompanySectorActivity(String companySectorActivity) {
    this.companySectorActivity = companySectorActivity;
  }

  public CompanyBean companyWebSite(String companyWebSite) {
    this.companyWebSite = companyWebSite;
    return this;
  }

  /**
   * Get companyWebSite
   * @return companyWebSite
  **/
  @ApiModelProperty(value = "")


  public String getCompanyWebSite() {
    return companyWebSite;
  }

  public void setCompanyWebSite(String companyWebSite) {
    this.companyWebSite = companyWebSite;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CompanyBean companyBean = (CompanyBean) o;
    return Objects.equals(this.companyId, companyBean.companyId) &&
        Objects.equals(this.account, companyBean.account) &&
        Objects.equals(this.companyName, companyBean.companyName) &&
        Objects.equals(this.companyDescription, companyBean.companyDescription) &&
        Objects.equals(this.companySectorActivity, companyBean.companySectorActivity) &&
        Objects.equals(this.companyWebSite, companyBean.companyWebSite);
  }

  @Override
  public int hashCode() {
    return Objects.hash(companyId, account, companyName, companyDescription, companySectorActivity, companyWebSite);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CompanyBean {\n");
    
    sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    companyName: ").append(toIndentedString(companyName)).append("\n");
    sb.append("    companyDescription: ").append(toIndentedString(companyDescription)).append("\n");
    sb.append("    companySectorActivity: ").append(toIndentedString(companySectorActivity)).append("\n");
    sb.append("    companyWebSite: ").append(toIndentedString(companyWebSite)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

