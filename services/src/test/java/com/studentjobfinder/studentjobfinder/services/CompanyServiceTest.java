package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.*;
import com.studentjobfinder.studentjobfinder.dto.in.CompanyBean;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import com.studentjobfinder.studentjobfinder.repository.CompanyRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CompanyServiceTest {
    @Autowired
    AccountService accountService;

    @Autowired
    CompanyService companyService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private CompanyRepository companyRepository;

    @Test
    public void addCompanyTest() {
        Account account = new Account("myCompany@mail.com", "password", LocalDateTime.now(), AccountRole.COMPANY, AccountStatus.UNVERIFIED);
        Company company = new Company(account, "myCompany", "", "", "");

        Mockito.when(companyRepository.save(Mockito.any(Company.class))).thenReturn(company);
        Mockito.when(accountRepository.findById(Mockito.anyString())).thenReturn(Optional.of(account));

        CompanyBean companyBean = new CompanyBean();
        companyBean.setAccountId("1");
        companyBean.setName("myCompany");
        companyBean.setDescription("");
        companyBean.setSectorActivity("");
        companyBean.setWebsite("");
        Company addedCompany = companyService.addCompany(company);

        assertEquals(company, addedCompany);
    }
}
