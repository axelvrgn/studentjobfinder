import { Box, Link, Stack, Text } from "@chakra-ui/react";
import { NavLink } from "react-router-dom";
import PageContainer from "../../../layouts/PageContainer";
import FormLogin from "../../../components/Authentication/Login/LoginForm";

function LoginView() {
  return (
    <PageContainer>
      <Box
        w="lg"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        px={8}
        py={4}
        mb={4}
        mx={"auto"}
        className="flex flex-col items-center"
      >
        <Text fontSize="3xl" align="center" mb={3}>
          Connexion
        </Text>
        <FormLogin />
      </Box>
      <Box
        w="lg"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        p={4}
        mx={"auto"}
      >
        <Stack direction={"row"}>
          <Text>Vous n'avez pas de compte ?</Text>{" "}
          <Link as={NavLink} to={"/register"} color={"purple.600"}>
            Inscrivez-vous
          </Link>
        </Stack>
      </Box>
    </PageContainer>
  );
}

export default LoginView;
