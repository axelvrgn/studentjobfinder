package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.util.Collection;

@Document("Account")
@Getter
@Setter
public class Account implements UserDetails {

    @Id
    private String accountId;
    @Indexed(unique = true)
    private String accountEmail;
    private String accountPassword;
    private LocalDateTime accountCreationDate;
    private AccountRole accountRole;
    private AccountStatus accountStatus;

    public Account(String accountEmail, String accountPassword, LocalDateTime accountCreationDate, AccountRole accountRole, AccountStatus accountStatus){
        this.accountEmail = accountEmail;
        this.accountPassword = accountPassword;
        this.accountCreationDate = accountCreationDate;
        this.accountRole = accountRole;
        this.accountStatus = accountStatus;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() { return accountRole.getAuthorities(); }

    @Override
    public String getPassword() {
        return accountPassword;
    }

    @Override
    public String getUsername() {
        return accountEmail;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
