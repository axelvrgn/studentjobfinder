import {
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { MESSAGE_REQUIRED } from "../../../../data/constants";
import { useState } from "react";

type FormRegisterCompanyProps = {
  createAccountCompany: (name: string, email: string, password: string) => void;
  registerButtonIsLoading: boolean;
};

interface IFormRegisterCompany {
  name: string;
  email: string;
  password: string;
  passwordVerif: string;
}

function FormRegisterCompany({
  createAccountCompany,
  registerButtonIsLoading,
}: FormRegisterCompanyProps) {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors, isValid },
  } = useForm<IFormRegisterCompany>({
    mode: "onBlur",
  });

  const saveCompany = (formRegisterCompanyValues: IFormRegisterCompany) => {
    createAccountCompany(
      formRegisterCompanyValues.name,
      formRegisterCompanyValues.email,
      formRegisterCompanyValues.password
    );
  };

  const handleClickShowPassword = () => setShowPassword(!showPassword);

  return (
    <form className="flex flex-col" onSubmit={handleSubmit(saveCompany)}>
      <FormControl mb={3} isRequired>
        <Input
          {...register("name", { required: true })}
          type="text"
          placeholder="Nom de l'entreprise"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.email && errors.email.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.email} isRequired>
        <Input
          id="email"
          {...register("email", { required: MESSAGE_REQUIRED })}
          type="mail"
          placeholder="Adresse e-mail"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.email && errors.email.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.password} isRequired>
        <InputGroup size="md">
          <Input
            {...register("password", { required: MESSAGE_REQUIRED })}
            type={showPassword ? "text" : "password"}
            placeholder="Mot de passe"
            focusBorderColor="purple.500"
            pr="5rem"
          />
          <InputRightElement width="4.5rem" mr="0.2rem">
            <Button h="1.75rem" size="sm" onClick={handleClickShowPassword}>
              {showPassword ? "Masquer" : "Afficher"}
            </Button>
          </InputRightElement>
        </InputGroup>
        <FormHelperText>Nécessite au moins 8 caractères</FormHelperText>
        <FormErrorMessage>
          {errors.password && errors.password.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.passwordVerif} isRequired>
        <Input
          id="passwordVerif"
          {...register("passwordVerif", {
            required: MESSAGE_REQUIRED,
            validate: (val: string) => {
              if (watch("password") != val) {
                return "Les mots de passe doivent être identique";
              }
            },
          })}
          type="password"
          placeholder="Confirmation du mot de passe"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.passwordVerif && errors.passwordVerif.message}
        </FormErrorMessage>
      </FormControl>
      <Button
        type="submit"
        colorScheme="purple"
        mt={3}
        isLoading={registerButtonIsLoading}
        isDisabled={!isValid}
      >
        S'inscrire
      </Button>
    </form>
  );
}

export default FormRegisterCompany;
