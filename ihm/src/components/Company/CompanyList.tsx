import { Stack } from "@chakra-ui/react";
import { CompanyBean } from "../../services/interfaces-generated/STUDENTJOBFINDER-INTERFACES";
import CompanyBox from "./CompanyBox";

type CompanyListProps = {
  companies: CompanyBean[];
};

function CompanyList({ companies }: CompanyListProps) {
  return (
    <Stack direction={"column"} w={"full"} spacing={4}>
      {companies.map((company) => (
        <CompanyBox key={company.companyId} company={company} />
      ))}
    </Stack>
  );
}

export default CompanyList;
