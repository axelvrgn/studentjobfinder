import { Text } from "@chakra-ui/react";

type TitleProps = {
  title: string;
  fontSize?: string;
  underlined?: boolean;
};

function Title({ title, fontSize, underlined }: TitleProps) {
  return (
    <div className="w-full flex flex-col mb-8">
      <Text fontSize={fontSize ?? "3xl"} className="font-medium">
        {title}
      </Text>
      {underlined && (
        <hr className="border border-purple-600 rounded-full w-1/12 bg-purple-600" />
      )}
    </div>
  );
}

export default Title;
