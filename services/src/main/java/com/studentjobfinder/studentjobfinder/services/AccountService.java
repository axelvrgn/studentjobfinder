package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import com.studentjobfinder.studentjobfinder.Model.AccountStatus;
import com.studentjobfinder.studentjobfinder.Model.Account;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountService implements IAccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public Account addAccount(Account account){
        return accountRepository.save(account);
    }

    @Override
    public Account updateAccountStatut(String accountId, AccountStatus accountStatus) {
        Account account = findAccountByIdOrThrow(accountId);
        account.setAccountStatus(accountStatus);
        return accountRepository.save(account);
    }

    @Override
    public Account updateAccountRole(String accountId, AccountRole accountRole) {
        Account account = findAccountByIdOrThrow(accountId);
        account.setAccountRole(accountRole);
        return accountRepository.save(account);
    }

    @Override
    public Account findAccountByIdOrThrow(String accountId) throws ResourceNotFoundException {
        return accountRepository.findById(accountId).orElseThrow(() -> new ResourceNotFoundException("Compte introuvable"));
    }

    @Override
    public Boolean existAccountByEmail(String email) {
        return accountRepository.existsAccountByAccountEmail(email);
    }
}
