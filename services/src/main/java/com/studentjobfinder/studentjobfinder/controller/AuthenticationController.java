package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.studentjobfinder.services.AccountService;
import com.studentjobfinder.studentjobfinder.services.LogoutService;
import com.studentjobfinder.studentjobfinder.dto.in.AccountBean;
import com.studentjobfinder.studentjobfinder.dto.in.LoginBean;
import com.studentjobfinder.studentjobfinder.dto.out.AuthenticationBean;
import com.studentjobfinder.studentjobfinder.services.AuthenticationService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    AccountService accountService;

    @Autowired
    LogoutService logoutService;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody AccountBean accountBean) {
        if(accountService.existAccountByEmail(accountBean.getEmail())){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Cet adresse e-mail est déjà utilisée");
        }
        return ResponseEntity.ok(authenticationService.register(accountBean.getEmail(), accountBean.getPassword(), accountBean.getRole()));
    }
    

    @PostMapping("/authenticate")
    public ResponseEntity<AuthenticationBean> authenticate(@RequestBody LoginBean loginBean) {
        return ResponseEntity.ok(authenticationService.authenticate(loginBean));
    }

    @PostMapping("/refresh-token")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        authenticationService.refreshToken(request, response);
    }

}
