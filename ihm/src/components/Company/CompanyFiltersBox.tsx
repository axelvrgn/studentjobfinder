import {
  Box,
  Button,
  ButtonGroup,
  FormControl,
  Input,
  Select,
  Stack,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";

type FormFilterJobProps = {
  sendFilter: (keyword: string, location: string, contractType: string) => void;
};

interface IFormFilterJob {
  keyword: string;
  location: string;
  sectorActivity: string;
}

function CompanyFiltersBox({ sendFilter }: FormFilterJobProps) {
  const { register, handleSubmit } = useForm<IFormFilterJob>();

  const submitFilters = (formFilterJobValues: IFormFilterJob) => {
    sendFilter(
      formFilterJobValues.keyword,
      formFilterJobValues.location,
      formFilterJobValues.sectorActivity
    );
  };

  return (
    <Box
      w="full"
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      px={8}
      py={4}
      mb={4}
      mx={"auto"}
    >
      <form
        className="flex flex-col items-end"
        onSubmit={handleSubmit(submitFilters)}
      >
        <Stack direction={"row"} w={"full"} spacing={4}>
          <FormControl>
            <Input
              {...register("keyword")}
              type="text"
              placeholder="Recherchez une entreprise, mot-clé..."
            />
          </FormControl>
          <FormControl>
            <Input {...register("location")} type="text" placeholder="Lieu" />
          </FormControl>
          <FormControl>
            <Select
              {...register("sectorActivity")}
              placeholder="Secteur d'activité"
            />
          </FormControl>
        </Stack>
        <ButtonGroup mt={3}>
          <Button type="submit" colorScheme="purple">
            Rechercher
          </Button>
          <Button type="submit" variant={"outline"} colorScheme="purple">
            Effacer les filtres
          </Button>
        </ButtonGroup>
      </form>
    </Box>
  );
}

export default CompanyFiltersBox;
