import { Box, Text, Stack, Link } from "@chakra-ui/react";
import { NavLink } from "react-router-dom";
import PageContainer from "../../../layouts/PageContainer";
import FormRegister from "../../../components/Authentication/Register/RegisterForm";

function RegisterView() {
  return (
    <PageContainer>
      <Box
        w="lg"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        px={8}
        py={4}
        mb={4}
        mx={"auto"}
        className="flex flex-col items-center"
      >
        <Text fontSize="3xl" align="center" mb={3}>
          Inscription
        </Text>
        <FormRegister />
      </Box>
      <Box
        w="lg"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        p={4}
        mx={"auto"}
      >
        <Stack direction={"row"}>
          <Text>Vous avez un compte ?</Text>{" "}
          <Link as={NavLink} to={"/login"} color={"purple.600"}>
            Connectez-vous
          </Link>
        </Stack>
      </Box>
    </PageContainer>
  );
}

export default RegisterView;
