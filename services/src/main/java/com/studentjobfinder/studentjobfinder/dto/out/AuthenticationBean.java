package com.studentjobfinder.studentjobfinder.dto.out;

import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import lombok.*;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AuthenticationBean {
    private String accessToken;
    private String refreshToken;
    private AccountRole accountRole;
    private String accountId;
}
