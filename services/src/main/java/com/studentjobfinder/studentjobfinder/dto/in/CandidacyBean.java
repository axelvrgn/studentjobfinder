package com.studentjobfinder.studentjobfinder.dto.in;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CandidacyBean {
    private String studentId;
    private String jobId;
    private String coverLetter;
}
