import { useEffect, useState } from "react";
import { jobService } from "../../services/jobService";
import { PageableJobBean } from "../../services/interfaces-generated/STUDENTJOBFINDER-INTERFACES";
import PageContainer from "../../layouts/PageContainer";
import Title from "../../components/Title";
import { Stack } from "@chakra-ui/react";
import Pagination from "../../components/Pagination";
import Loader from "../../components/Loader";
import JobFiltersBox from "../../components/job/JobFiltersBox";
import JobList from "../../components/job/jobList";

export function HomeView() {
  const [jobs, setJobs] = useState<PageableJobBean | null>(null);
  const [keyword, setKeyword] = useState<string>("");
  const [location, setLocation] = useState<string>("");
  const [type, setType] = useState<string>("");
  const [jobsIsLoading, setJobsIsLoading] = useState<boolean>(false);

  useEffect(() => {
    fetchJobs(keyword, location, type);
  }, [keyword, location, type]);

  const applyFilters = (
    keyword: string,
    location: string,
    contractType: string
  ) => {
    setKeyword(keyword);
    setLocation(location);
    setType(contractType);
  };

  const removeFilters = () => {
    setKeyword("");
    setLocation("");
    setType("");
  };

  const fetchJobs = (keyword: string, location: string, type: string) => {
    setJobsIsLoading(true);
    jobService
      .getJobsPaginated({ keyword, location, type, page: 0, size: 10 })
      .then((jobsRes) => {
        setJobs(jobsRes.data);
      })
      .finally(() => {
        setJobsIsLoading(false);
      });
  };

  return (
    <PageContainer>
      <Stack direction={"row"} w={"full"} justifyContent={"space-between"}>
        <div className="w-4/12">
          <Title fontSize={"3xl"} title="Dernières offres" underlined />
          <JobFiltersBox
            sendFilters={applyFilters}
            removeFilters={removeFilters}
          />
        </div>
        {jobs === null || jobsIsLoading ? (
          <Loader withContainer />
        ) : (
          <div className="w-7/12">
            <JobList jobs={jobs.content} />
            {jobs.content.length > 0 && (
              <Pagination pageNumber={1} count={10} totalCount={100} />
            )}
          </div>
        )}
      </Stack>
    </PageContainer>
  );
}
