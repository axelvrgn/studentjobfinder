package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.studentjobfinder.Model.CandidacyStatus;
import com.studentjobfinder.studentjobfinder.Model.Job;
import com.studentjobfinder.studentjobfinder.Model.Student;
import com.studentjobfinder.studentjobfinder.dto.in.CandidacyBean;
import com.studentjobfinder.studentjobfinder.Model.Candidacy;
import com.studentjobfinder.studentjobfinder.services.CandidacyService;
import com.studentjobfinder.studentjobfinder.services.JobService;
import com.studentjobfinder.studentjobfinder.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/candidacy")
public class CandidacyController {
    @Autowired
    CandidacyService candidacyService;

    @Autowired
    StudentService studentService;

    @Autowired
    JobService jobService;

    @GetMapping("/getCandidacyById")
    public ResponseEntity<Candidacy> getCandidacyById(@RequestParam String candidacyId) {
        return ResponseEntity.ok(candidacyService.getCandidacyById(candidacyId));
    }

    @GetMapping("/getCandidaciesByStudentIdPaginated")
    public Page<Candidacy> getCandidaciesByStudentIdPaginated(@RequestParam String studentId, @RequestParam Integer page, @RequestParam Integer size) {
        return candidacyService.getCandidaciesByStudentIdPaginated(studentId, page, size);
    }

    @GetMapping("/getCandidaciesByJobIdPaginated")
    public Page<Candidacy> getCandidaciesByJobIdPaginated(@RequestParam String jobId, @RequestParam Integer page, @RequestParam Integer size) { return candidacyService.getCandidaciesByJobIdPaginated(jobId, page, size); }

    @PostMapping("/addCandidacy")
    public ResponseEntity<String> addCandidacy(@RequestBody CandidacyBean newCandidacy){
        if(candidacyService.checkIfExistCandidacyByJobIdAndStudentId(newCandidacy.getJobId(), newCandidacy.getStudentId())){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Vous avez déjà candidaté à ce poste");
        }
        Student student = studentService.findStudentByIdOrThrow(newCandidacy.getStudentId());
        Job job = jobService.findJobByIdOrThrow(newCandidacy.getJobId());
        Candidacy candidacy = new Candidacy(student, job, newCandidacy.getCoverLetter(), LocalDateTime.now(), CandidacyStatus.UNVIEWED);
        candidacyService.addCandidacy(candidacy);
        return ResponseEntity.status(HttpStatus.CREATED).body("Candidature enregistrée avec succès");
    }
    @PatchMapping("/updateCandidacyStatusToViewed")
        public ResponseEntity<String> updateCandidacyStatusToViewed(@RequestParam String candidacyId) {
        candidacyService.updateCandidacyStatut(candidacyId, CandidacyStatus.VIEWED);
        return ResponseEntity.ok("Le statut de la candidature a été mis à jour (vue)");
    }

    @PatchMapping("/updateCandidacyStatusToAccepted")
    public ResponseEntity<String> updateCandidacyStatusToAccepted(@RequestParam String candidacyId) {
        candidacyService.updateCandidacyStatut(candidacyId, CandidacyStatus.ACCEPTED);
        return ResponseEntity.ok("Le statut de la candidature a été mis à jour (accepté)");
    }

    @PatchMapping("/updateCandidacyStatusToRefused")
    public ResponseEntity<String> updateCandidacyStatusToRefused(@RequestParam String candidacyId) {
        candidacyService.updateCandidacyStatut(candidacyId, CandidacyStatus.REFUSED);
        return ResponseEntity.ok("Le statut de la candidature a été mis à jour (refusé)");
    }

    @DeleteMapping("/deleteCandidacyByStatusUnviewed")
    public ResponseEntity<String> deleteCandidacyByStatusUnviewed(String candidacyId) {
        Candidacy candidacyToDelete = candidacyService.findCandidacyByIdOrThrow(candidacyId);
        if(!candidacyToDelete.getCandidacyStatus().equals(CandidacyStatus.UNVIEWED)){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Cette candidature à été vue par l'employeur, elle ne peut pas être supprimée");
        }
        candidacyService.deleteCandidacyUnviewed(candidacyId);
        return ResponseEntity.ok("Candidature supprimée avec succès");
    }
}
