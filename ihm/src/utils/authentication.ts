export const userIsLogged = () => {
  if (getUserToken()) {
    return true;
  }
  return false;
};

export const getUserToken = () => {
  return sessionStorage.getItem("sjf_user_token");
};

export const saveUserToken = (token: string) => {
  sessionStorage.setItem("sjf_user_token", token);
};
