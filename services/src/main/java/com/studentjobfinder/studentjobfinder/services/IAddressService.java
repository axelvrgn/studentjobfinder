package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Address;

import java.util.Optional;

public interface IAddressService {
    Address addAddress(Address address);
    Optional<Address> findAddressByAddressAndCityAndPostalCode(String address, String city, String postalCode);
    Address findAddressByIdOrThrow(String addressId);
}
