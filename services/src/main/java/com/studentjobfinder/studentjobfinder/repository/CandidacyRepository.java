package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Candidacy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CandidacyRepository extends MongoRepository<Candidacy, String> {

    Page<Candidacy> findCandidaciesByStudentStudentId(String studentId, Pageable pageable);
    Page<Candidacy> findCandidaciesByJobJobId(String jobId, Pageable pageable);
    boolean existsByJob_JobIdAndStudent_StudentId(String jobId, String studentId);
}
