package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Student;

import java.util.List;

public interface IStudentService {
    List<Student> getAllStudents();
    Student findStudentByAccountIdOrThrow(String accountId);
    Student addStudent(Student student);
    Student updateStudent(Student student);
    boolean checkIfExistStudentByAccountId(String accountId);
    Student findStudentByIdOrThrow(String studentId);
}
