import {
  Button,
  FormControl,
  FormErrorMessage,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { useToasts } from "../../../contexts/toast";
import { useForm } from "react-hook-form";
import { authenticateService } from "../../../services/authenticateService";
import { saveUserToken } from "../../../utils/authentication";
import { useState } from "react";
import { ERROR, MESSAGE_REQUIRED, SUCCESS } from "../../../data/constants";
import { useNavigate } from "react-router-dom";

interface ILoginForm {
  email: string;
  password: string;
}

function FormLogin() {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [formLoginIsLoading, setformLoginIsLoading] = useState<boolean>(false);
  const { pushToast } = useToasts();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<ILoginForm>({
    mode: "onBlur",
  });

  const authenticate = (loginFormValues: ILoginForm) => {
    setformLoginIsLoading(true);
    authenticateService
      .authenticate({
        email: loginFormValues.email,
        password: loginFormValues.password,
      })
      .then((res) => {
        saveUserToken(res.data.accessToken);
        navigate("/");
        pushToast({ content: "Vous êtes connecté", type: SUCCESS });
      })
      .catch((err) => {
        pushToast({ content: err.response.data, type: ERROR });
      })
      .finally(() => {
        setformLoginIsLoading(false);
      });
  };

  const handleClickShowPassword = () => setShowPassword(!showPassword);
  return (
    <form className="flex flex-col w-80" onSubmit={handleSubmit(authenticate)}>
      <FormControl mb={3} isInvalid={!!errors.email} isRequired>
        <Input
          {...register("email", { required: MESSAGE_REQUIRED })}
          type="mail"
          placeholder="Adresse e-mail"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.email && errors.email.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.password} isRequired>
        <InputGroup size="md">
          <Input
            {...register("password", { required: MESSAGE_REQUIRED })}
            type={showPassword ? "text" : "password"}
            placeholder="Mot de passe"
            focusBorderColor="purple.500"
            pr="5rem"
          />
          <InputRightElement width="4.5rem" mr="0.2rem">
            <Button h="1.75rem" size="sm" onClick={handleClickShowPassword}>
              {showPassword ? "Masquer" : "Afficher"}
            </Button>
          </InputRightElement>
        </InputGroup>
        <FormErrorMessage>
          {errors.password && errors.password.message}
        </FormErrorMessage>
      </FormControl>
      <Button
        type="submit"
        colorScheme="purple"
        mt={3}
        isLoading={formLoginIsLoading}
        isDisabled={!isValid}
      >
        Connexion
      </Button>
    </form>
  );
}

export default FormLogin;
