/**
 * NOTE: This class is auto generated by the swagger code generator program (2.3.1).
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */
package com.studentjobfinder.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-23T17:02:46.256+01:00")

@Api(value = "account", description = "the account API")
public interface AccountApi {

    @ApiOperation(value = "update account status to banned", nickname = "updateAccountStatusToBanned", notes = "permet de modifier le statut d'un compte à banni", response = String.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = String.class) })
    @RequestMapping(value = "/account/updateAccountStatusToBanned",
        produces = { "application/json" }, 
        method = RequestMethod.PATCH)
    ResponseEntity<String> updateAccountStatusToBanned();


    @ApiOperation(value = "update account status to blocked", nickname = "updateAccountStatusToBlocked", notes = "permet de modifier le statut d'un compte à bloqué", response = String.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = String.class) })
    @RequestMapping(value = "/account/updateAccountStatusToBlocked",
        produces = { "application/json" }, 
        method = RequestMethod.PATCH)
    ResponseEntity<String> updateAccountStatusToBlocked();


    @ApiOperation(value = "update account status to verified", nickname = "updateAccountStatusToVerified", notes = "permet de modifier le statut d'un compte à vérifié", response = String.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = String.class) })
    @RequestMapping(value = "/account/updateAccountStatusToVerified",
        produces = { "application/json" }, 
        method = RequestMethod.PATCH)
    ResponseEntity<String> updateAccountStatusToVerified();


    @ApiOperation(value = "update account type to admin", nickname = "updateAccountTypeToAdmin", notes = "permet de modifier le type d'un compte à admin", response = String.class, tags={  })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "successful operation", response = String.class) })
    @RequestMapping(value = "/account/updateAccountTypeToAdmin",
        produces = { "application/json" }, 
        method = RequestMethod.PATCH)
    ResponseEntity<String> updateAccountTypeToAdmin();

}
