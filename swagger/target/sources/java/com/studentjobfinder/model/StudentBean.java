package com.studentjobfinder.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.studentjobfinder.model.AccountBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * StudentBean
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-23T17:02:46.256+01:00")

public class StudentBean   {
  @JsonProperty("studentId")
  private String studentId = null;

  @JsonProperty("account")
  private AccountBean account = null;

  @JsonProperty("studentFirstname")
  private String studentFirstname = null;

  @JsonProperty("studentLastname")
  private String studentLastname = null;

  @JsonProperty("studentDescription")
  private String studentDescription = null;

  @JsonProperty("studentPhoneNumber")
  private String studentPhoneNumber = null;

  @JsonProperty("studentPortfolio")
  private String studentPortfolio = null;

  @JsonProperty("studentSkills")
  @Valid
  private List<String> studentSkills = null;

  public StudentBean studentId(String studentId) {
    this.studentId = studentId;
    return this;
  }

  /**
   * Get studentId
   * @return studentId
  **/
  @ApiModelProperty(value = "")


  public String getStudentId() {
    return studentId;
  }

  public void setStudentId(String studentId) {
    this.studentId = studentId;
  }

  public StudentBean account(AccountBean account) {
    this.account = account;
    return this;
  }

  /**
   * Get account
   * @return account
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AccountBean getAccount() {
    return account;
  }

  public void setAccount(AccountBean account) {
    this.account = account;
  }

  public StudentBean studentFirstname(String studentFirstname) {
    this.studentFirstname = studentFirstname;
    return this;
  }

  /**
   * Get studentFirstname
   * @return studentFirstname
  **/
  @ApiModelProperty(value = "")


  public String getStudentFirstname() {
    return studentFirstname;
  }

  public void setStudentFirstname(String studentFirstname) {
    this.studentFirstname = studentFirstname;
  }

  public StudentBean studentLastname(String studentLastname) {
    this.studentLastname = studentLastname;
    return this;
  }

  /**
   * Get studentLastname
   * @return studentLastname
  **/
  @ApiModelProperty(value = "")


  public String getStudentLastname() {
    return studentLastname;
  }

  public void setStudentLastname(String studentLastname) {
    this.studentLastname = studentLastname;
  }

  public StudentBean studentDescription(String studentDescription) {
    this.studentDescription = studentDescription;
    return this;
  }

  /**
   * Get studentDescription
   * @return studentDescription
  **/
  @ApiModelProperty(value = "")


  public String getStudentDescription() {
    return studentDescription;
  }

  public void setStudentDescription(String studentDescription) {
    this.studentDescription = studentDescription;
  }

  public StudentBean studentPhoneNumber(String studentPhoneNumber) {
    this.studentPhoneNumber = studentPhoneNumber;
    return this;
  }

  /**
   * Get studentPhoneNumber
   * @return studentPhoneNumber
  **/
  @ApiModelProperty(value = "")


  public String getStudentPhoneNumber() {
    return studentPhoneNumber;
  }

  public void setStudentPhoneNumber(String studentPhoneNumber) {
    this.studentPhoneNumber = studentPhoneNumber;
  }

  public StudentBean studentPortfolio(String studentPortfolio) {
    this.studentPortfolio = studentPortfolio;
    return this;
  }

  /**
   * Get studentPortfolio
   * @return studentPortfolio
  **/
  @ApiModelProperty(value = "")


  public String getStudentPortfolio() {
    return studentPortfolio;
  }

  public void setStudentPortfolio(String studentPortfolio) {
    this.studentPortfolio = studentPortfolio;
  }

  public StudentBean studentSkills(List<String> studentSkills) {
    this.studentSkills = studentSkills;
    return this;
  }

  public StudentBean addStudentSkillsItem(String studentSkillsItem) {
    if (this.studentSkills == null) {
      this.studentSkills = new ArrayList<String>();
    }
    this.studentSkills.add(studentSkillsItem);
    return this;
  }

  /**
   * Get studentSkills
   * @return studentSkills
  **/
  @ApiModelProperty(value = "")


  public List<String> getStudentSkills() {
    return studentSkills;
  }

  public void setStudentSkills(List<String> studentSkills) {
    this.studentSkills = studentSkills;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    StudentBean studentBean = (StudentBean) o;
    return Objects.equals(this.studentId, studentBean.studentId) &&
        Objects.equals(this.account, studentBean.account) &&
        Objects.equals(this.studentFirstname, studentBean.studentFirstname) &&
        Objects.equals(this.studentLastname, studentBean.studentLastname) &&
        Objects.equals(this.studentDescription, studentBean.studentDescription) &&
        Objects.equals(this.studentPhoneNumber, studentBean.studentPhoneNumber) &&
        Objects.equals(this.studentPortfolio, studentBean.studentPortfolio) &&
        Objects.equals(this.studentSkills, studentBean.studentSkills);
  }

  @Override
  public int hashCode() {
    return Objects.hash(studentId, account, studentFirstname, studentLastname, studentDescription, studentPhoneNumber, studentPortfolio, studentSkills);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class StudentBean {\n");
    
    sb.append("    studentId: ").append(toIndentedString(studentId)).append("\n");
    sb.append("    account: ").append(toIndentedString(account)).append("\n");
    sb.append("    studentFirstname: ").append(toIndentedString(studentFirstname)).append("\n");
    sb.append("    studentLastname: ").append(toIndentedString(studentLastname)).append("\n");
    sb.append("    studentDescription: ").append(toIndentedString(studentDescription)).append("\n");
    sb.append("    studentPhoneNumber: ").append(toIndentedString(studentPhoneNumber)).append("\n");
    sb.append("    studentPortfolio: ").append(toIndentedString(studentPortfolio)).append("\n");
    sb.append("    studentSkills: ").append(toIndentedString(studentSkills)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

