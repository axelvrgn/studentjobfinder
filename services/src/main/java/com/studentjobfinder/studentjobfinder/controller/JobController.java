package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.model.AddressBean;
import com.studentjobfinder.model.JobBean;
import com.studentjobfinder.model.PageableJobBean;
import com.studentjobfinder.studentjobfinder.Model.*;
import com.studentjobfinder.studentjobfinder.services.IAddressService;
import com.studentjobfinder.studentjobfinder.services.ICompanyService;
import com.studentjobfinder.studentjobfinder.services.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/job")
public class JobController {

    @Autowired
    IJobService jobService;

    @Autowired
    ICompanyService companyService;

    @Autowired
    IAddressService addressService;

    final DateFormat DATE_FORMAT_WITHOUT_TIME = new SimpleDateFormat("dd-MM-yyyy");
    final DateTimeFormatter DATE_FORMAT_WITH_TIME = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");

    @PostMapping("/addJob")
    public ResponseEntity<String> addJob(@RequestBody JobBean jobBean){
        Company company = companyService.findCompanyByIdOrThrow(jobBean.getCompanyId());

        Optional<Address> optionalAddress = addressService.findAddressByAddressAndCityAndPostalCode(
                jobBean.getJobAddress().getAddress(),
                jobBean.getJobAddress().getAddressCity(),
                jobBean.getJobAddress().getAddressPostalCode());

        Address address;
        if(optionalAddress.isEmpty()){
            Address newAddress = new Address(
                    jobBean.getJobAddress().getAddress(),
                    jobBean.getJobAddress().getAddressCity(),
                    jobBean.getJobAddress().getAddressPostalCode()
            );
            address = addressService.addAddress(newAddress);
        }
        else{
            address = optionalAddress.get();
        }

        Job job = new Job(
                company,
                jobBean.getJobTitle(),
                jobBean.getJobDescription(),
                LocalDateTime.now(),
                Date.valueOf(jobBean.getJobStartDate()),
                jobBean.getJobSalary(),
                JobType.valueOf(jobBean.getJobType()),
                jobBean.getJobAdvantages(),
                address,
                JobStatus.UNVERIFIED);

        jobService.addJob(job);

        return ResponseEntity.status(HttpStatus.CREATED).body("Nouvelle offre d'emplois enregistrée avec succès");
    }

    @GetMapping("/getAllJobs")
    public List<JobBean> getAllJobs() {
        List<JobBean> jobBeanList = new ArrayList<>();
        for (Job job: jobService.getAllJobs()){
           jobBeanList.add(jobToJobBeanMapper(job));
        }
        return jobBeanList;
    }

    @GetMapping("/getJobsPaginated")
    public ResponseEntity<PageableJobBean> getJobsPaginated(@RequestParam @Nullable String keyword, @RequestParam @Nullable String location, @RequestParam @Nullable String type, @RequestParam Integer page, @RequestParam Integer size) {
        var pageableJobBean = new PageableJobBean();
        pageableJobBean = jobsPaginatedToPageableJobBeanMapper(jobService.getJobsByKeywordAndLocationAndTypePaginated(keyword, location, type, page, size));
        return ResponseEntity.ok(pageableJobBean);
    }

    @GetMapping("/getJobsByCompanyPaginated")
    public ResponseEntity<PageableJobBean> getJobsByCompanyPaginated(@RequestParam String companyId, @RequestParam Integer page, @RequestParam Integer size) {
        var pageableJobBean = jobsPaginatedToPageableJobBeanMapper(jobService.getJobsByCompanyIdPaginated(companyId, page, size));
        return ResponseEntity.ok(pageableJobBean);
    }

    @GetMapping("/getJobsByStatusVerifiedPaginated")
    public ResponseEntity<PageableJobBean> getJobsByStatusVerifiedPaginated(@RequestParam Integer page, @RequestParam Integer size) {
        var pageableJobBean = jobsPaginatedToPageableJobBeanMapper(jobService.getJobsByStatusPaginated(JobStatus.VERIFIED.name(), page, size));
        return ResponseEntity.ok(pageableJobBean);
    }

    @PatchMapping("/updateJob")
    public ResponseEntity<String> updateJob(@RequestBody JobBean jobBean) {
        Job job = jobService.findJobByIdOrThrow(jobBean.getJobId());
        job.setJobTitle(jobBean.getJobTitle());
        job.setJobDescription(jobBean.getJobDescription());
        job.setJobStartDate(Date.valueOf(jobBean.getJobStartDate()));
        job.setJobSalary(jobBean.getJobSalary());
        job.setJobType(JobType.valueOf(jobBean.getJobType()));
        job.setJobAdvantages(jobBean.getJobAdvantages());
        jobService.updateJob(job);
        return ResponseEntity.ok("Offre d'emplois mise à jour avec succès");
    }

    @PatchMapping("/updateJobStatusToVerified")
    public ResponseEntity<String> updateJobStatusToVerified(@RequestParam String jobId) {
        jobService.updateJobStatutFromJobId(jobId, JobStatus.VERIFIED);
        return ResponseEntity.ok("Statut de l'offre d'emplois mis à jour avec succès");
    }

    @PatchMapping("/updateJobStatusToClosed")
    public ResponseEntity<String> updateJobStatusToClosed(@RequestParam String jobId) {
        jobService.updateJobStatutFromJobId(jobId, JobStatus.CLOSED);
        return ResponseEntity.ok("Statut de l'offre d'emplois mis à jour avec succès");
    }

    @DeleteMapping("/deleteJobByStatusUnverified")
    public ResponseEntity<String> deleteJobByStatusUnverified(@RequestParam String jobId) {
        Job job = jobService.findJobByIdOrThrow(jobId);
        if(!job.getJobStatus().equals(JobStatus.UNVERIFIED)){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Cette offre d'emplois à été publiée, elle ne peut pas être supprimée");
        }
        jobService.deleteJobById(jobId);
        return ResponseEntity.ok("L'offre d'emplois à été supprimée avec succès");
    }

    private JobBean jobToJobBeanMapper(Job job) {
        JobBean jobBean = new JobBean();

        jobBean.setJobId(job.getJobId());
        jobBean.setCompanyId(job.getCompany().getCompanyId());
        jobBean.setCompanyName(job.getCompany().getCompanyName());
        jobBean.setJobTitle(job.getJobTitle());
        jobBean.setJobDescription(job.getJobDescription());
        jobBean.setJobCreationDate(job.getJobCreationDate().format(DATE_FORMAT_WITH_TIME));
        jobBean.setJobStartDate(DATE_FORMAT_WITHOUT_TIME.format(job.getJobStartDate()));
        jobBean.setJobSalary(job.getJobSalary());
        jobBean.setJobType(job.getJobType() != null ? job.getJobType().name() : null);
        jobBean.setJobAdvantages(job.getJobAdvantages());

        if(job.getAddress() != null) {
            AddressBean addressBean = new AddressBean();
            addressBean.setAddress(job.getAddress().getAddressAddress());
            addressBean.setAddressCity(job.getAddress().getAddressCity());
            addressBean.setAddressPostalCode(job.getAddress().getAddressPostalCode());
            jobBean.setJobAddress(addressBean);
        }

        jobBean.setJobStatus(job.getJobStatus().name());

        return jobBean;
    }

    private PageableJobBean jobsPaginatedToPageableJobBeanMapper(Page<Job> jobsPaginated) {
        List<JobBean> jobBeanList = new ArrayList<>();
        for (Job job: jobsPaginated.getContent()){
            jobBeanList.add(jobToJobBeanMapper(job));
        }
        PageableJobBean pageableJobBean = new PageableJobBean();
        pageableJobBean.setContent(jobBeanList);
        pageableJobBean.setPageNumber(jobsPaginated.getNumber());
        pageableJobBean.setPageSize(jobsPaginated.getSize());
        pageableJobBean.setNumberOfElements(jobsPaginated.getNumberOfElements());
        pageableJobBean.setTotalPages(jobsPaginated.getTotalPages());
        pageableJobBean.setTotalElements((int) jobsPaginated.getTotalElements());
        return pageableJobBean;
    }
}
