package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Job;
import com.studentjobfinder.studentjobfinder.Model.JobStatus;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IJobService {
    Job addJob(Job job);
    Page<Job> getJobsByCompanyIdPaginated(String companyId, Integer page, Integer size);
    Page<Job> getJobsPaginated(Integer page, Integer size);
    Page<Job> getJobsByKeywordAndLocationAndTypePaginated(String keyword, String location, String type, Integer page, Integer size);
    Page<Job> getJobsByStatusPaginated(String jobStatus, Integer page, Integer size);
    List<Job> getAllJobs();
    Job updateJobStatutFromJobId(String jobId, JobStatus jobStatus);
    Job updateJob(Job jobUpdated);
    void deleteJobById(String jobId);
    Job findJobByIdOrThrow(String jobId);
}
