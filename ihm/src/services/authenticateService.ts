import Axios from "./callerService";
import {
  AuthenticationBean,
  LoginBean,
} from "./interfaces-generated/STUDENTJOBFINDER-INTERFACES";

const authenticate = async ({ email, password }: LoginBean) => {
  return Axios.post<AuthenticationBean>("/auth/authenticate", {
    email,
    password,
  });
};

type accountBean = {
  email: string;
  password: string;
  role: string;
};
const register = async ({ email, password, role }: accountBean) => {
  return Axios.post("/auth/register", {
    email,
    password,
    role,
  });
};

export const authenticateService = {
  authenticate,
  register,
};
