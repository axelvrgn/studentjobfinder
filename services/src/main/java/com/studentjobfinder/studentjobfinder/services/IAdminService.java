package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Admin;

public interface IAdminService {
    Admin findAdminByIdOrThrow(String adminId);
}
