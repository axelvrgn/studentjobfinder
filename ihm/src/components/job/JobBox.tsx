import { Box, HStack, Tag, Text } from "@chakra-ui/react";
import { JobBean } from "../../services/interfaces-generated/STUDENTJOBFINDER-INTERFACES";
import Title from "../Title";
import Icon from "@mdi/react";
import { mdiMapMarker } from "@mdi/js";
import { PURPLE700_HEXA } from "../../data/constants";
import moment from "moment";

type jobBoxProps = {
  job: JobBean;
};

function JobBox({ job }: jobBoxProps) {
  const start = moment(job.jobCreationDate).format("DD/MM/YYYY");
  const end = moment(new Date());

  const duration = moment.duration(end.diff(start));

  return (
    <Box borderWidth="1px" p={3} borderRadius="lg">
      <Text fontSize={"sm"}>{job.companyName}</Text>
      <Title title={job.jobTitle} fontSize={"xl"} />
      <HStack spacing={2}>
        {job.jobType && (
          <Tag size={"sm"} padding={"1"} variant="outline" colorScheme="purple">
            <Text>{job.jobType}</Text>
          </Tag>
        )}
        {job.jobAddress && (
          <Tag size={"sm"} padding={"1"} variant="outline" colorScheme="purple">
            <Icon path={mdiMapMarker} size={0.5} color={PURPLE700_HEXA} />
            <Text>{job.jobAddress?.addressCity}</Text>
          </Tag>
        )}
        {job.jobSalary && (
          <Tag size={"sm"} padding={"1"} variant="outline" colorScheme="purple">
            <Text>{job.jobSalary}</Text>
          </Tag>
        )}
      </HStack>
      <HStack spacing={1}>
        <Text fontSize={"xs"}>
          il y a {duration.days()} jour{duration.days() > 1 && "s"}
        </Text>
      </HStack>
    </Box>
  );
}

export default JobBox;
