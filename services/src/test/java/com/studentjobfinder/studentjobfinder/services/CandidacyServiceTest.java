package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.*;
import com.studentjobfinder.studentjobfinder.dto.in.CandidacyBean;
import com.studentjobfinder.studentjobfinder.repository.*;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CandidacyServiceTest {

    @Autowired
    CandidacyService candidacyService;

    @MockBean
    CandidacyRepository candidacyRepository;

    @MockBean
    AccountRepository accountRepository;

    @MockBean
    StudentRepository studentRepository;

    @MockBean
    JobRepository jobRepository;

    @MockBean
    CompanyRepository companyRepository;

    @Test
    public void addCandidacyTest() {
        Account account = new Account(
                "myCompany@mail.com",
                "password",
                LocalDateTime.now(),
                AccountRole.COMPANY,
                AccountStatus.UNVERIFIED);

        Student student = new Student(
                account,
                "john",
                "doe",
                "",
                "",
                "",
                null,
                null);

        Company company = new Company(
                account,
                "MyCompany",
                "",
                "",
                "");

        Job job = new Job(
                company,
                "Developpeur back-end",
                "poste de developpeur",
                LocalDateTime.now(),
                Date.valueOf("2023-01-01"),
                "2000$/month",
                JobType.CDI,
                Arrays.asList("self restaurant"),
                new Address(
                        "10 rue des peupliers",
                        "Paris",
                        "75001"),
                JobStatus.UNVERIFIED);

        Candidacy candidacy = new Candidacy(
                student,
                job,
                "",
                LocalDateTime.now(),
                CandidacyStatus.UNVIEWED);

        Mockito.when(studentRepository.findById(Mockito.any())).thenReturn(Optional.of(student));
        Mockito.when(jobRepository.findById(Mockito.any())).thenReturn(Optional.of(job));
        Mockito.when(candidacyRepository.save(Mockito.any(Candidacy.class))).thenReturn(candidacy);

        CandidacyBean candidacyBean = new CandidacyBean();
        candidacyBean.setJobId(account.getAccountId());
        candidacyBean.setStudentId(student.getStudentId());
        candidacyBean.setCoverLetter("");

        Candidacy addedCandidacy = candidacyService.addCandidacy(candidacy);

        assertEquals(candidacy, addedCandidacy);
    }
}
