package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface AddressRepository extends MongoRepository<Address, String> {
    Optional<Address> findAddressByAddressAddressAndAddressCityAndAddressPostalCode(String address, String city, String postalCode);

}
