import { Box, HStack, Stack } from "@chakra-ui/react";
import { CompanyBean } from "../../services/interfaces-generated/STUDENTJOBFINDER-INTERFACES";
import Title from "../Title";
import CompanyAvatar from "./CompanyAvatar";

type CompanyBoxProps = {
  company: CompanyBean;
};

function CompanyBox({ company }: CompanyBoxProps) {
  return (
    <Box borderWidth="1px" p={3} borderRadius="lg">
      <HStack spacing={4}>
        <CompanyAvatar />
        <Stack direction={"column"}>
          <Title title={company.companyName} fontSize={"xl"} />
        </Stack>
      </HStack>
    </Box>
  );
}

export default CompanyBox;
