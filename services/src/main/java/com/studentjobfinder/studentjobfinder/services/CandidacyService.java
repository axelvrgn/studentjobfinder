package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.CandidacyStatus;
import com.studentjobfinder.studentjobfinder.Model.Candidacy;
import com.studentjobfinder.studentjobfinder.repository.CandidacyRepository;
import com.studentjobfinder.studentjobfinder.repository.JobRepository;
import com.studentjobfinder.studentjobfinder.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CandidacyService implements ICandidacyService {
    @Autowired
    StudentRepository studentRepository;

    @Autowired
    JobRepository jobRepository;

    @Autowired
    CandidacyRepository candidacyRepository;

    @Override
    public Candidacy getCandidacyById(String candidacyId) {
        return candidacyRepository.findById(candidacyId).orElseThrow(() -> new ResourceNotFoundException("Candidature introuvable avec l'identifiant " + candidacyId));
    }

    @Override
    public Page<Candidacy> getCandidaciesByStudentIdPaginated(String studentId, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return candidacyRepository.findCandidaciesByStudentStudentId(studentId, paging);
    }

    @Override
    public Page<Candidacy> getCandidaciesByJobIdPaginated(String jobId, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return candidacyRepository.findCandidaciesByJobJobId(jobId, paging);
    }

    @Override
    public Candidacy addCandidacy(Candidacy candidacy) {
        return candidacyRepository.save(candidacy);
    }

    @Override
    public Candidacy updateCandidacyStatut (String candidacyId, CandidacyStatus candidacyStatus) {
        Candidacy candidacy = findCandidacyByIdOrThrow(candidacyId);
        candidacy.setCandidacyStatus(candidacyStatus);
        return candidacyRepository.save(candidacy);
    }

    @Override
    public void deleteCandidacyUnviewed(String candidacyId) {
        candidacyRepository.deleteById(candidacyId);
    }

    @Override
    public boolean checkIfExistCandidacyByJobIdAndStudentId (String jobId, String studentId) {
        return candidacyRepository.existsByJob_JobIdAndStudent_StudentId(jobId, studentId);
    }

    @Override
    public Candidacy findCandidacyByIdOrThrow (String candidacyId) throws ResourceNotFoundException {
        return candidacyRepository.findById(candidacyId).orElseThrow(() -> new ResourceNotFoundException("Candidature introuvable"));
    }
}
