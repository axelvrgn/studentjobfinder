package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.model.StudentBean;
import com.studentjobfinder.studentjobfinder.Model.Account;
import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import com.studentjobfinder.studentjobfinder.Model.Student;
import com.studentjobfinder.studentjobfinder.services.IAccountService;
import com.studentjobfinder.studentjobfinder.services.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    IStudentService studentService;

    @Autowired
    IAccountService accountService;

    @GetMapping("/getAllStudents")
    public List<Student> getAllStudents(){
        return studentService.getAllStudents();
    }

    @GetMapping("/getStudentByAccountId")
    public ResponseEntity<Student> getStudentByAccountId(@RequestParam String accountId) {
        return ResponseEntity.ok(studentService.findStudentByAccountIdOrThrow(accountId));
    }

    @PostMapping("/addStudent")
    public ResponseEntity<String> addStudent(@RequestBody StudentBean newStudent){
        Account account  = accountService.findAccountByIdOrThrow(newStudent.getAccount().getAccountId());
        if(!account.getAccountRole().equals(AccountRole.STUDENT)){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Ce compte n'est pas référencé comme un profil étudiant");
        }
        if(studentService.checkIfExistStudentByAccountId(newStudent.getAccount().getAccountId())){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Le profil étudiant de ce compte a déjà été créé");
        }
        Student student = new Student(account, newStudent.getStudentFirstname(), newStudent.getStudentLastname(), newStudent.getStudentDescription(), newStudent.getStudentPhoneNumber(), newStudent.getStudentPortfolio(), null, null);
        studentService.addStudent(student);
        return ResponseEntity.ok("Nouveau profil étudiant enregistré avec succès");
    }

    @PatchMapping("/updateStudent")
    public ResponseEntity<String> updateStudent(@RequestBody StudentBean updatedStudent){
        Student student = studentService.findStudentByIdOrThrow(updatedStudent.getStudentId());
        student.setStudentFirstname(updatedStudent.getStudentFirstname());
        student.setStudentLastname(updatedStudent.getStudentLastname());
        studentService.updateStudent(student);
        return ResponseEntity.ok("Profil étudiant mis à jour avec succès");
    }
}
