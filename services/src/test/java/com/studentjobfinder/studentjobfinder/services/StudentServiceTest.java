package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.*;
import com.studentjobfinder.studentjobfinder.dto.in.StudentBean;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import com.studentjobfinder.studentjobfinder.repository.StudentRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.util.Optional;

@SpringBootTest
public class StudentServiceTest {

    @Autowired
    AccountService accountService;

    @Autowired
    StudentService studentService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private StudentRepository studentRepository;

    @Test
    public void addStudentTest() {
        Account account = new Account("john.doe@gmail.com", "password", LocalDateTime.now(), AccountRole.STUDENT, AccountStatus.UNVERIFIED);
        Student student = new Student(account, "john", "doe", "", "", "", null,  null);

        Mockito.when(accountRepository.findById(Mockito.anyString())).thenReturn(Optional.of(account));
        Mockito.when(studentRepository.save(Mockito.any(Student.class))).thenReturn(student);

        StudentBean studentBean = new StudentBean();
        studentBean.setAccountId("1");
        studentBean.setFirstname("John");
        studentBean.setLastname("Doe");
        studentBean.setDescrition("");

        Student addedStudent = studentService.addStudent(student);

        assertEquals(student, addedStudent);
    }
}
