package com.studentjobfinder.studentjobfinder.Model;

public enum JobStatus {
    UNVERIFIED, VERIFIED, CLOSED
}
