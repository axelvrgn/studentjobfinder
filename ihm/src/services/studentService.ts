import Axios from "./callerService";
import { StudentBean } from "./interfaces-generated/STUDENTJOBFINDER-INTERFACES";

type studentBean = {
  accountId: string;
  firstname: string;
  lastname: string;
};

const addStudent = async ({ accountId, firstname, lastname }: studentBean) => {
  return Axios.post("/student/addStudent", { accountId, firstname, lastname });
};

const getAllStudents = async () => {
  return Axios.get<StudentBean[]>("/student/getAllStudents");
};

const getStudentByAccountId = async (accountId: string) => {
  return Axios.get<StudentBean>("/student/getStudentByAccountId", {
    params: { accountId },
  });
};

const updateStudent = async (student: StudentBean) => {
  return Axios.patch("/student/updateStudent", student);
};

export const studentService = {
  addStudent,
  getAllStudents,
  getStudentByAccountId,
  updateStudent,
};
