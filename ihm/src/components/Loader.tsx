import { Container, Spinner } from "@chakra-ui/react";

type LoaderProps = {
  withContainer?: boolean;
};

function Loader({ withContainer }: LoaderProps) {
  if (withContainer) {
    return (
      <Container
        w={"full"}
        h={"400px"}
        className="flex"
        alignItems={"center"}
        justifyContent={"center"}
      >
        <Spinner size={"lg"} color="purple.600" />
      </Container>
    );
  }
  return <Spinner size={"lg"} color="purple.600" />;
}

export default Loader;
