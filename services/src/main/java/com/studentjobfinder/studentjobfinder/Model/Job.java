package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Document("Job")
@Getter
@Setter
public class Job {
    @Id
    private String jobId;
    @DBRef
    private Company company;
    private String jobTitle;
    private String jobDescription;
    private LocalDateTime jobCreationDate;
    private Date jobStartDate;
    private String jobSalary;
    private JobType jobType;
    private List<String> jobAdvantages;
    @DBRef
    private Address address;
    private JobStatus jobStatus;

    public Job(Company company, String jobTitle, String jobDescription, LocalDateTime jobCreationDate, Date jobStartDate, String jobSalary, JobType jobType, List<String> jobAdvantages, Address address, JobStatus jobStatus){
        this.company = company;
        this.jobTitle = jobTitle;
        this.jobDescription = jobDescription;
        this.jobCreationDate = jobCreationDate;
        this.jobStartDate = jobStartDate;
        this.jobSalary = jobSalary;
        this.jobType = jobType;
        this.jobAdvantages = jobAdvantages;
        this.address = address;
        this.jobStatus = jobStatus;
    }

}
