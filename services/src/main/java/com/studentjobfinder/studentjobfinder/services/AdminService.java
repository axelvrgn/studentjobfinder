package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Admin;
import com.studentjobfinder.studentjobfinder.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AdminService implements IAdminService {
    @Autowired
    AdminRepository adminRepository;

    @Override
    public Admin findAdminByIdOrThrow(String adminId) throws ResourceNotFoundException {
        return adminRepository.findById(adminId).orElseThrow(() -> new ResourceNotFoundException("Admin introuvable"));
    }
}
