package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.studentjobfinder.Model.Admin;
import com.studentjobfinder.studentjobfinder.services.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/admin")
public class AdminController {
    @Autowired
    IAdminService adminService;
    @GetMapping("/getById")
    public ResponseEntity<Admin> getAdminById(@RequestParam String adminId) {
        return ResponseEntity.ok(adminService.findAdminByIdOrThrow(adminId));
    }
}
