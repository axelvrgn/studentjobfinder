import { Image } from "@chakra-ui/react";
import noImage from "../../assets/no_image.jpeg";

const CompanyAvatar = () => {
  return (
    <Image
      src={noImage}
      boxSize={"120px"}
      objectFit={"cover"}
      borderRadius={"lg"}
    />
  );
};

export default CompanyAvatar;
