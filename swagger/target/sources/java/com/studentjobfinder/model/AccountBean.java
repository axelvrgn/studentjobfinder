package com.studentjobfinder.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * AccountBean
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-23T17:02:46.256+01:00")

public class AccountBean   {
  @JsonProperty("accountId")
  private String accountId = null;

  @JsonProperty("accountEmail")
  private String accountEmail = null;

  @JsonProperty("accountPassword")
  private String accountPassword = null;

  @JsonProperty("accountCreationDate")
  private String accountCreationDate = null;

  /**
   * Gets or Sets accountRole
   */
  public enum AccountRoleEnum {
    ADMIN("ADMIN"),
    
    STUDENT("STUDENT"),
    
    COMPANY("COMPANY");

    private String value;

    AccountRoleEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AccountRoleEnum fromValue(String text) {
      for (AccountRoleEnum b : AccountRoleEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("accountRole")
  private AccountRoleEnum accountRole = null;

  /**
   * Gets or Sets accountStatus
   */
  public enum AccountStatusEnum {
    UNVERIFIED("UNVERIFIED"),
    
    VERIFIED("VERIFIED"),
    
    BLOCKED("BLOCKED"),
    
    BANNED("BANNED");

    private String value;

    AccountStatusEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static AccountStatusEnum fromValue(String text) {
      for (AccountStatusEnum b : AccountStatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("accountStatus")
  private AccountStatusEnum accountStatus = null;

  public AccountBean accountId(String accountId) {
    this.accountId = accountId;
    return this;
  }

  /**
   * Get accountId
   * @return accountId
  **/
  @ApiModelProperty(value = "")


  public String getAccountId() {
    return accountId;
  }

  public void setAccountId(String accountId) {
    this.accountId = accountId;
  }

  public AccountBean accountEmail(String accountEmail) {
    this.accountEmail = accountEmail;
    return this;
  }

  /**
   * Get accountEmail
   * @return accountEmail
  **/
  @ApiModelProperty(value = "")


  public String getAccountEmail() {
    return accountEmail;
  }

  public void setAccountEmail(String accountEmail) {
    this.accountEmail = accountEmail;
  }

  public AccountBean accountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
    return this;
  }

  /**
   * Get accountPassword
   * @return accountPassword
  **/
  @ApiModelProperty(value = "")


  public String getAccountPassword() {
    return accountPassword;
  }

  public void setAccountPassword(String accountPassword) {
    this.accountPassword = accountPassword;
  }

  public AccountBean accountCreationDate(String accountCreationDate) {
    this.accountCreationDate = accountCreationDate;
    return this;
  }

  /**
   * Get accountCreationDate
   * @return accountCreationDate
  **/
  @ApiModelProperty(value = "")


  public String getAccountCreationDate() {
    return accountCreationDate;
  }

  public void setAccountCreationDate(String accountCreationDate) {
    this.accountCreationDate = accountCreationDate;
  }

  public AccountBean accountRole(AccountRoleEnum accountRole) {
    this.accountRole = accountRole;
    return this;
  }

  /**
   * Get accountRole
   * @return accountRole
  **/
  @ApiModelProperty(value = "")


  public AccountRoleEnum getAccountRole() {
    return accountRole;
  }

  public void setAccountRole(AccountRoleEnum accountRole) {
    this.accountRole = accountRole;
  }

  public AccountBean accountStatus(AccountStatusEnum accountStatus) {
    this.accountStatus = accountStatus;
    return this;
  }

  /**
   * Get accountStatus
   * @return accountStatus
  **/
  @ApiModelProperty(value = "")


  public AccountStatusEnum getAccountStatus() {
    return accountStatus;
  }

  public void setAccountStatus(AccountStatusEnum accountStatus) {
    this.accountStatus = accountStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AccountBean accountBean = (AccountBean) o;
    return Objects.equals(this.accountId, accountBean.accountId) &&
        Objects.equals(this.accountEmail, accountBean.accountEmail) &&
        Objects.equals(this.accountPassword, accountBean.accountPassword) &&
        Objects.equals(this.accountCreationDate, accountBean.accountCreationDate) &&
        Objects.equals(this.accountRole, accountBean.accountRole) &&
        Objects.equals(this.accountStatus, accountBean.accountStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountId, accountEmail, accountPassword, accountCreationDate, accountRole, accountStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AccountBean {\n");
    
    sb.append("    accountId: ").append(toIndentedString(accountId)).append("\n");
    sb.append("    accountEmail: ").append(toIndentedString(accountEmail)).append("\n");
    sb.append("    accountPassword: ").append(toIndentedString(accountPassword)).append("\n");
    sb.append("    accountCreationDate: ").append(toIndentedString(accountCreationDate)).append("\n");
    sb.append("    accountRole: ").append(toIndentedString(accountRole)).append("\n");
    sb.append("    accountStatus: ").append(toIndentedString(accountStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

