package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.studentjobfinder.studentjobfinder.Model.AccountPermission.*;

@RequiredArgsConstructor
public enum AccountRole {

    ADMIN(
            Set.of(
                    ADMIN_READ,
                    ADMIN_UPDATE,
                    ADMIN_DELETE,
                    ADMIN_CREATE
            )
    ),
    COMPANY(
            Set.of(
                    COMPANY_READ,
                    COMPANY_UPDATE,
                    COMPANY_DELETE,
                    COMPANY_CREATE
            )
    ),

    STUDENT(
        Set.of(
                STUDENT_READ,
                STUDENT_UPDATE,
                STUDENT_DELETE,
                STUDENT_CREATE
        )
    );

    @Getter
    private final Set<AccountPermission> permissions;

    public List<SimpleGrantedAuthority> getAuthorities() {
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }
}
