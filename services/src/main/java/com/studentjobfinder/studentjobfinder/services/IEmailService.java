package com.studentjobfinder.studentjobfinder.services;

import org.springframework.mail.SimpleMailMessage;

public interface IEmailService {

    void sendEmail(String email, String code);
}
