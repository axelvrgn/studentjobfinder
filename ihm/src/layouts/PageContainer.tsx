type PageContainerProps = {
  children?: JSX.Element | JSX.Element[];
};

function PageContainer({ children }: PageContainerProps) {
  return (
    <div className="page-container mx-auto py-4 dark:bg-slate-800 dark:text-white">
      {children}
    </div>
  );
}

export default PageContainer;
