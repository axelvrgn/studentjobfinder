package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Address")
@Getter
@Setter
public class Address {
    @Id
    private String addressId;
    private String addressAddress;
    private String addressCity;
    private String addressPostalCode;

    public Address(String addressAddress, String addressCity, String addressPostalCode) {
        this.addressAddress = addressAddress;
        this.addressCity = addressCity;
        this.addressPostalCode = addressPostalCode;
    }
}
