package com.studentjobfinder.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.studentjobfinder.model.AddressBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * JobBean
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-23T17:02:46.256+01:00")

public class JobBean   {
  @JsonProperty("jobId")
  private String jobId = null;

  @JsonProperty("companyId")
  private String companyId = null;

  @JsonProperty("companyName")
  private String companyName = null;

  @JsonProperty("jobTitle")
  private String jobTitle = null;

  @JsonProperty("jobDescription")
  private String jobDescription = null;

  @JsonProperty("jobCreationDate")
  private String jobCreationDate = null;

  @JsonProperty("jobStartDate")
  private String jobStartDate = null;

  @JsonProperty("jobSalary")
  private String jobSalary = null;

  @JsonProperty("jobType")
  private String jobType = null;

  @JsonProperty("jobAdvantages")
  @Valid
  private List<String> jobAdvantages = null;

  @JsonProperty("jobAddress")
  private AddressBean jobAddress = null;

  @JsonProperty("jobStatus")
  private String jobStatus = null;

  public JobBean jobId(String jobId) {
    this.jobId = jobId;
    return this;
  }

  /**
   * Get jobId
   * @return jobId
  **/
  @ApiModelProperty(value = "")


  public String getJobId() {
    return jobId;
  }

  public void setJobId(String jobId) {
    this.jobId = jobId;
  }

  public JobBean companyId(String companyId) {
    this.companyId = companyId;
    return this;
  }

  /**
   * Get companyId
   * @return companyId
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getCompanyId() {
    return companyId;
  }

  public void setCompanyId(String companyId) {
    this.companyId = companyId;
  }

  public JobBean companyName(String companyName) {
    this.companyName = companyName;
    return this;
  }

  /**
   * Get companyName
   * @return companyName
  **/
  @ApiModelProperty(value = "")


  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }

  public JobBean jobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
    return this;
  }

  /**
   * Get jobTitle
   * @return jobTitle
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public JobBean jobDescription(String jobDescription) {
    this.jobDescription = jobDescription;
    return this;
  }

  /**
   * Get jobDescription
   * @return jobDescription
  **/
  @ApiModelProperty(value = "")


  public String getJobDescription() {
    return jobDescription;
  }

  public void setJobDescription(String jobDescription) {
    this.jobDescription = jobDescription;
  }

  public JobBean jobCreationDate(String jobCreationDate) {
    this.jobCreationDate = jobCreationDate;
    return this;
  }

  /**
   * Get jobCreationDate
   * @return jobCreationDate
  **/
  @ApiModelProperty(value = "")


  public String getJobCreationDate() {
    return jobCreationDate;
  }

  public void setJobCreationDate(String jobCreationDate) {
    this.jobCreationDate = jobCreationDate;
  }

  public JobBean jobStartDate(String jobStartDate) {
    this.jobStartDate = jobStartDate;
    return this;
  }

  /**
   * Get jobStartDate
   * @return jobStartDate
  **/
  @ApiModelProperty(value = "")


  public String getJobStartDate() {
    return jobStartDate;
  }

  public void setJobStartDate(String jobStartDate) {
    this.jobStartDate = jobStartDate;
  }

  public JobBean jobSalary(String jobSalary) {
    this.jobSalary = jobSalary;
    return this;
  }

  /**
   * Get jobSalary
   * @return jobSalary
  **/
  @ApiModelProperty(value = "")


  public String getJobSalary() {
    return jobSalary;
  }

  public void setJobSalary(String jobSalary) {
    this.jobSalary = jobSalary;
  }

  public JobBean jobType(String jobType) {
    this.jobType = jobType;
    return this;
  }

  /**
   * Get jobType
   * @return jobType
  **/
  @ApiModelProperty(value = "")


  public String getJobType() {
    return jobType;
  }

  public void setJobType(String jobType) {
    this.jobType = jobType;
  }

  public JobBean jobAdvantages(List<String> jobAdvantages) {
    this.jobAdvantages = jobAdvantages;
    return this;
  }

  public JobBean addJobAdvantagesItem(String jobAdvantagesItem) {
    if (this.jobAdvantages == null) {
      this.jobAdvantages = new ArrayList<String>();
    }
    this.jobAdvantages.add(jobAdvantagesItem);
    return this;
  }

  /**
   * Get jobAdvantages
   * @return jobAdvantages
  **/
  @ApiModelProperty(value = "")


  public List<String> getJobAdvantages() {
    return jobAdvantages;
  }

  public void setJobAdvantages(List<String> jobAdvantages) {
    this.jobAdvantages = jobAdvantages;
  }

  public JobBean jobAddress(AddressBean jobAddress) {
    this.jobAddress = jobAddress;
    return this;
  }

  /**
   * Get jobAddress
   * @return jobAddress
  **/
  @ApiModelProperty(value = "")

  @Valid

  public AddressBean getJobAddress() {
    return jobAddress;
  }

  public void setJobAddress(AddressBean jobAddress) {
    this.jobAddress = jobAddress;
  }

  public JobBean jobStatus(String jobStatus) {
    this.jobStatus = jobStatus;
    return this;
  }

  /**
   * Get jobStatus
   * @return jobStatus
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull


  public String getJobStatus() {
    return jobStatus;
  }

  public void setJobStatus(String jobStatus) {
    this.jobStatus = jobStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    JobBean jobBean = (JobBean) o;
    return Objects.equals(this.jobId, jobBean.jobId) &&
        Objects.equals(this.companyId, jobBean.companyId) &&
        Objects.equals(this.companyName, jobBean.companyName) &&
        Objects.equals(this.jobTitle, jobBean.jobTitle) &&
        Objects.equals(this.jobDescription, jobBean.jobDescription) &&
        Objects.equals(this.jobCreationDate, jobBean.jobCreationDate) &&
        Objects.equals(this.jobStartDate, jobBean.jobStartDate) &&
        Objects.equals(this.jobSalary, jobBean.jobSalary) &&
        Objects.equals(this.jobType, jobBean.jobType) &&
        Objects.equals(this.jobAdvantages, jobBean.jobAdvantages) &&
        Objects.equals(this.jobAddress, jobBean.jobAddress) &&
        Objects.equals(this.jobStatus, jobBean.jobStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(jobId, companyId, companyName, jobTitle, jobDescription, jobCreationDate, jobStartDate, jobSalary, jobType, jobAdvantages, jobAddress, jobStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class JobBean {\n");
    
    sb.append("    jobId: ").append(toIndentedString(jobId)).append("\n");
    sb.append("    companyId: ").append(toIndentedString(companyId)).append("\n");
    sb.append("    companyName: ").append(toIndentedString(companyName)).append("\n");
    sb.append("    jobTitle: ").append(toIndentedString(jobTitle)).append("\n");
    sb.append("    jobDescription: ").append(toIndentedString(jobDescription)).append("\n");
    sb.append("    jobCreationDate: ").append(toIndentedString(jobCreationDate)).append("\n");
    sb.append("    jobStartDate: ").append(toIndentedString(jobStartDate)).append("\n");
    sb.append("    jobSalary: ").append(toIndentedString(jobSalary)).append("\n");
    sb.append("    jobType: ").append(toIndentedString(jobType)).append("\n");
    sb.append("    jobAdvantages: ").append(toIndentedString(jobAdvantages)).append("\n");
    sb.append("    jobAddress: ").append(toIndentedString(jobAddress)).append("\n");
    sb.append("    jobStatus: ").append(toIndentedString(jobStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

