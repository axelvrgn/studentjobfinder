import { Image } from "@chakra-ui/react";
import logo from "../assets/Logo-PSD.png";

type LogoProps = {
  width?: number;
  height?: number;
};

function Logo({ width, height }: LogoProps) {
  return (
    <Image
      width={width ?? 100}
      height={height ?? 50}
      objectFit="contain"
      src={logo}
    />
  );
}

export default Logo;
