package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.*;
import com.studentjobfinder.studentjobfinder.dto.in.JobBean;
import com.studentjobfinder.studentjobfinder.repository.CompanyRepository;
import com.studentjobfinder.studentjobfinder.repository.JobRepository;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class JobServiceTest {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    JobService jobService;

    @Autowired
    JobRepository jobRepository;

    public void addJobTest(){
        Account account = new Account(
                "myCompany@gmail.com",
                "password",
                LocalDateTime.now(),
                AccountRole.COMPANY,
                AccountStatus.UNVERIFIED);

        Company company = new Company(
                account,
                "myCompany",
                "",
                "",
                "");

        Job job = new Job(
                company,
                "Developpeur back-end",
                "poste de developpeur",
                LocalDateTime.now(),
                Date.valueOf("2023-01-01"),
                "2000$/month",
                JobType.CDI,
                Arrays.asList("self restaurant"),
                new Address(
                        "10 rue des peupliers",
                        "Paris",
                        "75001"),
                JobStatus.UNVERIFIED);

        Mockito.when(companyRepository.findById(Mockito.any())).thenReturn(Optional.of(company));
        Mockito.when(jobRepository.save(Mockito.any(Job.class))).thenReturn(job);

        JobBean jobBean = new JobBean();
        jobBean.setCompanyId("id");
        jobBean.setJobTitle("Developeur back-end");
        jobBean.setJobDescription("poste de developpeur");
        jobBean.setJobStartDate("2023-01-01");

        Job addedJob = jobService.addJob(job);

        assertEquals(job, addedJob);
    }

}
