export const STUDENT = "STUDENT";
export const COMPANY = "COMPANY";

export const SUCCESS = "SUCCESS";
export const ERROR = "ERROR";
export const INFO = "INFO";

export const MESSAGE_REQUIRED = "Champ requis";

export const PURPLE700_HEXA = "#9333ea";

export const JOB_TYPES = ["CDI", "CDD", "STAGE", "ALTERNANCE", "TEMPS_PARTIEL"];
