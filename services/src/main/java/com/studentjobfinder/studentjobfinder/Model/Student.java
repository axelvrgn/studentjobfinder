package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.Nullable;

import java.util.List;

@Document("Student")
@Getter
@Setter
public class Student {

    @Id
    private String studentId;
    @DBRef
    private Account account;
    private String studentFirstname;
    private String studentLastname;
    private String studentDescription;
    @Nullable
    private String studentPhoneNumber;
    @Nullable
    private String studentPortfolio;
    @Nullable
    private List<Skill> studentSkills;
    @Nullable
    @DBRef
    private List<Job> studentFavJobs;

    public Student(Account account, String studentFirstname, String studentLastname, String studentDescription, String studentPhoneNumber, String studentPortfolio, List<Skill> studentSkills, List<Job> studentFavJobs ){
        this.account = account;
        this.studentFirstname = studentFirstname;
        this.studentLastname = studentLastname;
        this.studentDescription = studentDescription;
        this.studentPhoneNumber = studentPhoneNumber;
        this.studentPortfolio = studentPortfolio;
        this.studentSkills = studentSkills;
        this.studentFavJobs = studentFavJobs;
    }

}
