package com.studentjobfinder.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.studentjobfinder.model.JobBean;
import com.studentjobfinder.model.StudentBean;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * CandidacyBean
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-23T17:02:46.256+01:00")

public class CandidacyBean   {
  @JsonProperty("candidacyId")
  private String candidacyId = null;

  @JsonProperty("student")
  private StudentBean student = null;

  @JsonProperty("job")
  private JobBean job = null;

  @JsonProperty("candidacyCoverLetter")
  private String candidacyCoverLetter = null;

  @JsonProperty("candidacyCreationDate")
  private String candidacyCreationDate = null;

  @JsonProperty("candidacyStatus")
  private String candidacyStatus = null;

  public CandidacyBean candidacyId(String candidacyId) {
    this.candidacyId = candidacyId;
    return this;
  }

  /**
   * Get candidacyId
   * @return candidacyId
  **/
  @ApiModelProperty(value = "")


  public String getCandidacyId() {
    return candidacyId;
  }

  public void setCandidacyId(String candidacyId) {
    this.candidacyId = candidacyId;
  }

  public CandidacyBean student(StudentBean student) {
    this.student = student;
    return this;
  }

  /**
   * Get student
   * @return student
  **/
  @ApiModelProperty(value = "")

  @Valid

  public StudentBean getStudent() {
    return student;
  }

  public void setStudent(StudentBean student) {
    this.student = student;
  }

  public CandidacyBean job(JobBean job) {
    this.job = job;
    return this;
  }

  /**
   * Get job
   * @return job
  **/
  @ApiModelProperty(value = "")

  @Valid

  public JobBean getJob() {
    return job;
  }

  public void setJob(JobBean job) {
    this.job = job;
  }

  public CandidacyBean candidacyCoverLetter(String candidacyCoverLetter) {
    this.candidacyCoverLetter = candidacyCoverLetter;
    return this;
  }

  /**
   * Get candidacyCoverLetter
   * @return candidacyCoverLetter
  **/
  @ApiModelProperty(value = "")


  public String getCandidacyCoverLetter() {
    return candidacyCoverLetter;
  }

  public void setCandidacyCoverLetter(String candidacyCoverLetter) {
    this.candidacyCoverLetter = candidacyCoverLetter;
  }

  public CandidacyBean candidacyCreationDate(String candidacyCreationDate) {
    this.candidacyCreationDate = candidacyCreationDate;
    return this;
  }

  /**
   * Get candidacyCreationDate
   * @return candidacyCreationDate
  **/
  @ApiModelProperty(value = "")


  public String getCandidacyCreationDate() {
    return candidacyCreationDate;
  }

  public void setCandidacyCreationDate(String candidacyCreationDate) {
    this.candidacyCreationDate = candidacyCreationDate;
  }

  public CandidacyBean candidacyStatus(String candidacyStatus) {
    this.candidacyStatus = candidacyStatus;
    return this;
  }

  /**
   * Get candidacyStatus
   * @return candidacyStatus
  **/
  @ApiModelProperty(value = "")


  public String getCandidacyStatus() {
    return candidacyStatus;
  }

  public void setCandidacyStatus(String candidacyStatus) {
    this.candidacyStatus = candidacyStatus;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CandidacyBean candidacyBean = (CandidacyBean) o;
    return Objects.equals(this.candidacyId, candidacyBean.candidacyId) &&
        Objects.equals(this.student, candidacyBean.student) &&
        Objects.equals(this.job, candidacyBean.job) &&
        Objects.equals(this.candidacyCoverLetter, candidacyBean.candidacyCoverLetter) &&
        Objects.equals(this.candidacyCreationDate, candidacyBean.candidacyCreationDate) &&
        Objects.equals(this.candidacyStatus, candidacyBean.candidacyStatus);
  }

  @Override
  public int hashCode() {
    return Objects.hash(candidacyId, student, job, candidacyCoverLetter, candidacyCreationDate, candidacyStatus);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CandidacyBean {\n");
    
    sb.append("    candidacyId: ").append(toIndentedString(candidacyId)).append("\n");
    sb.append("    student: ").append(toIndentedString(student)).append("\n");
    sb.append("    job: ").append(toIndentedString(job)).append("\n");
    sb.append("    candidacyCoverLetter: ").append(toIndentedString(candidacyCoverLetter)).append("\n");
    sb.append("    candidacyCreationDate: ").append(toIndentedString(candidacyCreationDate)).append("\n");
    sb.append("    candidacyStatus: ").append(toIndentedString(candidacyStatus)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

