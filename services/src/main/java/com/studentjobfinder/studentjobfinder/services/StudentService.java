package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Student;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import com.studentjobfinder.studentjobfinder.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService implements IStudentService {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    AccountService accountService;

    @Autowired
    AccountRepository accountRepository;

    @Override
    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    @Override
    public Student findStudentByAccountIdOrThrow(String accountId) throws ResourceNotFoundException {
        return studentRepository.findStudentByAccountAccountId(accountId).orElseThrow(() -> new ResourceNotFoundException("Profil étudiant introuvable"));
    }

    @Override
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student updateStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public boolean checkIfExistStudentByAccountId(String accountId) {
        return studentRepository.existsStudentByAccount_AccountId(accountId);
    }

    @Override
    public Student findStudentByIdOrThrow(String studentId) throws ResourceNotFoundException {
       return studentRepository.findById(studentId).orElseThrow(() -> new ResourceNotFoundException("Étudiant introuvable"));
    }
}
