package com.studentjobfinder.studentjobfinder.dto.in;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;


@Getter
@Setter
public class StudentBean {
    private String studentId;
    private String accountId;
    private String firstname;
    private String lastname;
    private String descrition;
    @Nullable
    private String  phoneNumber;
    @Nullable
    private String portfolio;
}
