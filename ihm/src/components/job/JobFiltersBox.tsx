import {
  Box,
  Button,
  ButtonGroup,
  FormControl,
  Input,
  Select,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { JOB_TYPES } from "../../data/constants";

type FormFilterJobProps = {
  sendFilters: (
    keyword: string,
    location: string,
    contractType: string
  ) => void;
  removeFilters: () => void;
};

interface IFormFilterJob {
  keyword: string;
  location: string;
  contractType: string;
}

function JobFiltersBox({ sendFilters, removeFilters }: FormFilterJobProps) {
  const { register, handleSubmit, reset } = useForm<IFormFilterJob>();

  const submitFilters = (formFilterJobValues: IFormFilterJob) => {
    sendFilters(
      formFilterJobValues.keyword,
      formFilterJobValues.location,
      formFilterJobValues.contractType
    );
  };

  const resetFilters = () => {
    reset();
    removeFilters();
  };

  return (
    <Box
      w="full"
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      px={8}
      py={4}
      mb={4}
    >
      <form
        className="flex flex-col items-center"
        onSubmit={handleSubmit(submitFilters)}
      >
        <FormControl mb={3}>
          <Input
            {...register("keyword")}
            type="text"
            placeholder="Recherchez par job, mot-clé..."
            focusBorderColor="purple.500"
          />
        </FormControl>
        <FormControl my={3}>
          <Input
            {...register("location")}
            type="text"
            placeholder="Où ?"
            focusBorderColor="purple.500"
          />
        </FormControl>
        <FormControl my={3}>
          <Select
            {...register("contractType")}
            placeholder="Type de contrat"
            focusBorderColor="purple.500"
          >
            {JOB_TYPES.map((type) => (
              <option key={type} value={type}>
                {type}
              </option>
            ))}
          </Select>
        </FormControl>
        <ButtonGroup mt={3}>
          <Button type="submit" colorScheme="purple">
            Rechercher
          </Button>
          <Button
            type="button"
            onClick={resetFilters}
            variant={"outline"}
            colorScheme="purple"
          >
            Effacer les filtres
          </Button>
        </ButtonGroup>
      </form>
    </Box>
  );
}

export default JobFiltersBox;
