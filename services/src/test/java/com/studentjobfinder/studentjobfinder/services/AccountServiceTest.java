package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import com.studentjobfinder.studentjobfinder.Model.AccountStatus;
import com.studentjobfinder.studentjobfinder.dto.in.AccountBean;
import com.studentjobfinder.studentjobfinder.Model.Account;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class AccountServiceTest {
    @Autowired
    AccountService accountService;

    @MockBean
    private AccountRepository accountRepository;

    @Test
    public void addAccountTest() {
        Account account = new Account("john.doe@gmail.com", "password", LocalDateTime.now(), AccountRole.STUDENT, AccountStatus.UNVERIFIED);

        Mockito.when(accountRepository.save(Mockito.any(Account.class))).thenReturn(account);

        AccountBean accountBean = new AccountBean();
        accountBean.setEmail("john.doe@gmail.com");
        accountBean.setPassword("password");
        accountBean.setRole("STUDENT");

        Account accountAdded = accountService.addAccount(account);

        assertEquals(account, accountAdded);
    }
}
