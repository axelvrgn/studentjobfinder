package com.studentjobfinder.studentjobfinder.services;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService implements IEmailService {
    private JavaMailSender javaMailSender;

    @Override
    public void sendEmail(String email, String code) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(email);
        message.setSubject("Bienvenue sur Student Job Finder !");
        message.setText("Salut, voici le code de verification : "+code);
        javaMailSender.send(message);
    }
}
