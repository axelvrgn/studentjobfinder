package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Company;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ICompanyService {

    List<Company> getAllCompanies();
    Page<Company> getCompaniesPaginated(Integer page, Integer size);
    Page<Company> getCompaniesByStatusPaginated(String accountStatus, Integer page, Integer size);
    Company findCompanyByAccountIdOrThrow(String accountId);
    Company findCompanyByIdOrThrow (String companyId);
    Company addCompany(Company company);
    Company updateCompany(Company company);
    boolean checkIfExistCompanyByAccountId (String accountId);

}
