import { Stack, Text } from "@chakra-ui/react";
import { JobBean } from "../../services/interfaces-generated/STUDENTJOBFINDER-INTERFACES";
import JobBox from "./jobBox";

type JobListProps = {
  jobs: JobBean[];
};

function JobList({ jobs }: JobListProps) {
  return (
    <Stack direction={"column"} w={"full"} spacing={4}>
      {jobs.map((job) => (
        <JobBox key={job.jobId} job={job} />
      ))}
      {jobs.length === 0 && <Text textAlign={"center"}>Aucun résultat</Text>}
    </Stack>
  );
}

export default JobList;
