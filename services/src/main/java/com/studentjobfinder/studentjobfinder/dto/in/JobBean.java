package com.studentjobfinder.studentjobfinder.dto.in;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.lang.Nullable;

import java.util.Date;

@Getter
@Setter
public class JobBean {
    @Nullable
    private String jobId;
    @NotNull
    private String companyId;
    @NotNull
    private String jobTitle;
    @Nullable
    private String jobDescription;
    @NotNull
    private String jobStartDate;
}
