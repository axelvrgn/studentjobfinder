package com.studentjobfinder.studentjobfinder.Model;

public enum AccountStatus {
    UNVERIFIED, VERIFIED, BLOCKED, BANNED;
}
