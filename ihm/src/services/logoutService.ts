import Axios from "./callerService";

const logout = async () => {
  return Axios.post("/auth/logout");
};

export const logoutService = {
  logout,
};
