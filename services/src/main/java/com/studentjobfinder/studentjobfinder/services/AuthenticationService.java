package com.studentjobfinder.studentjobfinder.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.studentjobfinder.studentjobfinder.Model.*;
import com.studentjobfinder.studentjobfinder.dto.in.LoginBean;
import com.studentjobfinder.studentjobfinder.dto.out.AuthenticationBean;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import com.studentjobfinder.studentjobfinder.repository.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    TokenRepository tokenRepository;
    PasswordEncoder passwordEncoder;
    JwtService jwtService;
    AuthenticationManager authenticationManager;
    @Autowired
    EmailService emailService;

    public AuthenticationBean register(String email, String password, String role) {
        var account = new Account(email, passwordEncoder.encode(password), LocalDateTime.now(), AccountRole.valueOf(role), AccountStatus.UNVERIFIED);
        var savedAccount = accountRepository.save(account);
        var jwtToken = jwtService.generateToken(account);
        var refreshToken = jwtService.generateRefreshToken(account);
        saveAccountToken(savedAccount, jwtToken);
        emailService.sendEmail(email, jwtToken);
        return AuthenticationBean.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .accountId(account.getAccountId())
                .accountRole(account.getAccountRole())
                .build();
    }

    public AuthenticationBean authenticate(LoginBean loginBean) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginBean.getEmail(), loginBean.getPassword()));
        var account = accountRepository.findAccountByAccountEmail(loginBean.getEmail()).orElseThrow();
        var jwtToken = jwtService.generateToken(account);
        var refreshToken = jwtService.generateRefreshToken(account);
        revokeAllAccountTokens(account);
        saveAccountToken(account, jwtToken);
        return AuthenticationBean.builder()
                .accessToken(jwtToken)
                .refreshToken(refreshToken)
                .accountId(account.getAccountId())
                .accountRole(account.getAccountRole())
                .build();
    }

    private void saveAccountToken(Account account, String jwtToken) {
        var token = new Token(jwtToken, false, false, account);
        tokenRepository.save(token);
    }

    private void revokeAllAccountTokens(Account account) {
        var validAccountTokens = tokenRepository.findAllByAccount_AccountIdAndTokenExpiredIsFalseAndTokenRevokedIsFalse(account.getAccountId());
        if (validAccountTokens.isEmpty())
            return;
        validAccountTokens.forEach(token -> {
            token.setTokenExpired(true);
            token.setTokenRevoked(true);
        });
        tokenRepository.saveAll(validAccountTokens);
    }

    public void refreshToken(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws IOException {
        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        final String refreshToken;
        final String accountEmail;
        if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
            return;
        }
        refreshToken = authHeader.substring(7);
        accountEmail = jwtService.extractUsername(refreshToken);
        if (accountEmail != null) {
            var account = this.accountRepository.findAccountByAccountEmail(accountEmail).orElseThrow();
            if (jwtService.isTokenValid(refreshToken, account)) {
                var accessToken = jwtService.generateToken(account);
                revokeAllAccountTokens(account);
                saveAccountToken(account, accessToken);
                var authResponse = AuthenticationBean.builder()
                        .accessToken(accessToken)
                        .refreshToken(refreshToken)
                        .build();
                new ObjectMapper().writeValue(response.getOutputStream(), authResponse);
            }
        }
    }
}
