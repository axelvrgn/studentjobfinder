import { useEffect } from "react";
import { ChevronLeftIcon, ChevronRightIcon } from "@chakra-ui/icons";
import { Button, ButtonGroup } from "@chakra-ui/react";

type PaginationProps = {
  count: number;
  pageNumber: number;
  totalCount: number;
};

function Pagination({ pageNumber }: PaginationProps) {
  useEffect(() => {}, []);
  return (
    <div className="w-full flex justify-center mt-8">
      <ButtonGroup>
        <Button>
          <ChevronLeftIcon boxSize={6} />
        </Button>
        <Button colorScheme="purple">{pageNumber}</Button>
        <Button>
          <ChevronRightIcon boxSize={6} />
        </Button>
      </ButtonGroup>
    </div>
  );
}

export default Pagination;
