import { useEffect, useState } from "react";
import CompanyFiltersBox from "../../components/Company/CompanyFiltersBox";
import CompanyList from "../../components/Company/CompanyList";
import Title from "../../components/Title";
import PageContainer from "../../layouts/PageContainer";
import { companyService } from "../../services/companyService";
import { PageableCompanyBean } from "../../services/interfaces-generated/STUDENTJOBFINDER-INTERFACES";
import Loader from "../../components/Loader";
import Pagination from "../../components/Pagination";

function CompaniesView() {
  const [companies, setCompanies] = useState<PageableCompanyBean | null>(null);

  useEffect(() => {
    companyService
      .getCompaniesPaginated({ page: 0, size: 10 })
      .then((companiesRes) => {
        setCompanies(companiesRes.data);
      });
  }, []);

  const applyFilters = () => {
    return null;
  };

  return (
    <PageContainer>
      <Title fontSize={"3xl"} title="Trouver une entreprise" underlined />
      <CompanyFiltersBox sendFilter={applyFilters} />
      {companies != null ? (
        <CompanyList companies={companies.content} />
      ) : (
        <Loader withContainer />
      )}
      <Pagination pageNumber={1} count={10} totalCount={10} />
    </PageContainer>
  );
}

export default CompaniesView;
