package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document("Admin")
@Getter
@Setter
public class Admin {
    @Id
    private String adminId;
    @DBRef
    private Account account;
    private AdminLevel adminLevel;
    private String adminPseudo;
    private LocalDateTime adminCreationDate;


    public Admin(Account account, AdminLevel adminLevel, String adminPseudo, LocalDateTime adminCreationDate){
        this.account = account;
        this.adminLevel = adminLevel;
        this.adminPseudo = adminPseudo;
        this.adminCreationDate = adminCreationDate;
    }
}
