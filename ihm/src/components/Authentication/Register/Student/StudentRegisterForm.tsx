import {
  Button,
  FormControl,
  FormErrorMessage,
  FormHelperText,
  Input,
  InputGroup,
  InputRightElement,
  Stack,
} from "@chakra-ui/react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { MESSAGE_REQUIRED } from "../../../../data/constants";

type FormRegisterStudentProps = {
  createAccountStudent: (
    firstname: string,
    lastname: string,
    email: string,
    password: string
  ) => void;
  registerButtonIsLoading: boolean;
};

interface IFormRegisterStudent {
  firstname: string;
  lastname: string;
  email: string;
  password: string;
  passwordVerif: string;
}

function FormRegisterStudent({
  createAccountStudent,
  registerButtonIsLoading,
}: FormRegisterStudentProps) {
  const [showPassword, setShowPassword] = useState<boolean>(false);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors, isValid },
  } = useForm<IFormRegisterStudent>({
    mode: "onBlur",
  });

  const saveStudent = (formRegisterStudentValues: IFormRegisterStudent) => {
    createAccountStudent(
      formRegisterStudentValues.firstname,
      formRegisterStudentValues.lastname,
      formRegisterStudentValues.email,
      formRegisterStudentValues.password
    );
  };

  const handleClickShowPassword = () => setShowPassword(!showPassword);

  return (
    <form className="flex flex-col" onSubmit={handleSubmit(saveStudent)}>
      <Stack direction="row" mb={3}>
        <FormControl isInvalid={!!errors.firstname} isRequired>
          <Input
            {...register("firstname", { required: MESSAGE_REQUIRED })}
            type="text"
            placeholder="Prénom"
            focusBorderColor="purple.500"
          />
          <FormErrorMessage>
            {errors.firstname && errors.firstname.message}
          </FormErrorMessage>
        </FormControl>
        <FormControl isInvalid={!!errors.lastname} isRequired>
          <Input
            {...register("lastname", { required: MESSAGE_REQUIRED })}
            type="text"
            placeholder="Nom"
            focusBorderColor="purple.500"
          />
          <FormErrorMessage>
            {errors.lastname && errors.lastname.message}
          </FormErrorMessage>
        </FormControl>
      </Stack>
      <FormControl my={3} isInvalid={!!errors.email} isRequired>
        <Input
          id="email"
          {...register("email", { required: MESSAGE_REQUIRED })}
          type="mail"
          placeholder="Adresse e-mail"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.email && errors.email.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.password} isRequired>
        <InputGroup size="md">
          <Input
            {...register("password", { required: MESSAGE_REQUIRED })}
            type={showPassword ? "text" : "password"}
            placeholder="Mot de passe"
            focusBorderColor="purple.500"
            pr="5rem"
          />
          <InputRightElement width="4.5rem" mr="0.2rem">
            <Button h="1.75rem" size="sm" onClick={handleClickShowPassword}>
              {showPassword ? "Masquer" : "Afficher"}
            </Button>
          </InputRightElement>
        </InputGroup>
        <FormHelperText>Nécessite au moins 8 caractères</FormHelperText>
        <FormErrorMessage>
          {errors.password && errors.password.message}
        </FormErrorMessage>
      </FormControl>
      <FormControl my={3} isInvalid={!!errors.passwordVerif} isRequired>
        <Input
          id="passwordVerif"
          {...register("passwordVerif", {
            required: MESSAGE_REQUIRED,
            validate: (val: string) => {
              if (watch("password") != val) {
                return "Les mots de passe doivent être identique";
              }
            },
          })}
          type="password"
          placeholder="Confirmation du mot de passe"
          focusBorderColor="purple.500"
        />
        <FormErrorMessage>
          {errors.passwordVerif && errors.passwordVerif.message}
        </FormErrorMessage>
      </FormControl>
      <Button
        type="submit"
        colorScheme="purple"
        mt={3}
        isLoading={registerButtonIsLoading}
        isDisabled={!isValid}
      >
        S'inscrire
      </Button>
    </form>
  );
}

export default FormRegisterStudent;
