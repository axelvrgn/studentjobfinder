import {
  CheckCircleIcon,
  InfoOutlineIcon,
  WarningIcon,
} from "@chakra-ui/icons";
import { alertState } from "./Toast";

type ToastIconProps = {
  type: alertState;
};

export function ToastIcon({ type }: ToastIconProps) {
  switch (type) {
    case "SUCCESS":
      return <CheckCircleIcon color={"green.600"} boxSize={4} />;
    case "ERROR":
      return <WarningIcon color={"red.500"} />;
    case "INFO":
      return <InfoOutlineIcon color={"blue.600"} />;
    default:
      return null;
  }
}
