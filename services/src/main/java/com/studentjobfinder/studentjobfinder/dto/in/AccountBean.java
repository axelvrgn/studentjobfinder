package com.studentjobfinder.studentjobfinder.dto.in;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountBean {
    @NotBlank
    @Email
    private String email;
    @NotBlank
    private String password;
    private String role;
}
