package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Address;
import com.studentjobfinder.studentjobfinder.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService implements IAddressService {

    @Autowired
    AddressRepository addressRepository;

    @Override
    public Address addAddress(Address address){
        return addressRepository.save(address);
    }

    @Override
    public Optional<Address> findAddressByAddressAndCityAndPostalCode(String address, String city, String postalCode){
        return addressRepository.findAddressByAddressAddressAndAddressCityAndAddressPostalCode(address, city, postalCode);
    }

    @Override
    public Address findAddressByIdOrThrow(String addressId){
        return addressRepository.findById(addressId).orElseThrow(() -> new ResourceNotFoundException("Adresse introuvable"));
    }
}
