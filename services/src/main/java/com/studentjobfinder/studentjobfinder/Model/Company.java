package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Company")
@Getter
@Setter
public class Company {

    @Id
    private String companyId;
    @DBRef
    private Account account;
    private String companyName;
    private String companyDescription;
    private String companySectorActivity;
    private String companyWebSite;

    public Company(Account account, String companyName, String companyDescription, String companySectorActivity, String companyWebSite){
        this.account = account;
        this.companyName = companyName;
        this.companyDescription = companyDescription;
        this.companySectorActivity = companySectorActivity;
        this.companyWebSite = companyWebSite;
    }
}
