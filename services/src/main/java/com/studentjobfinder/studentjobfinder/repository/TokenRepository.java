package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Token;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface TokenRepository extends MongoRepository<Token, String> {
    List<Token> findAllByAccount_AccountIdAndTokenExpiredIsFalseAndTokenRevokedIsFalse(String accountId);
    Optional<Token> findByAccount_AccountId(String accountId);
    Optional<Token> findTokenByToken(String token);


}
