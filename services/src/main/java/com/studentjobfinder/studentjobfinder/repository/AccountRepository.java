package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface AccountRepository extends MongoRepository<Account, String> {
    Optional<Account> findAccountByAccountEmailAndAccountPassword(String accountEmail, String accountPassword);

    Optional<Account> findAccountByAccountEmail(String accountEmail);

    Boolean existsAccountByAccountEmail(String accountEmail);
    
}
