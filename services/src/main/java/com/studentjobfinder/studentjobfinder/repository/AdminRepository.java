package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Admin;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AdminRepository extends MongoRepository<Admin, String> {
}
