package com.studentjobfinder.studentjobfinder.controller;

import com.studentjobfinder.studentjobfinder.Model.Account;
import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import com.studentjobfinder.studentjobfinder.Model.AccountStatus;
import com.studentjobfinder.studentjobfinder.dto.in.CompanyBean;
import com.studentjobfinder.studentjobfinder.Model.Company;
import com.studentjobfinder.studentjobfinder.services.IAccountService;
import com.studentjobfinder.studentjobfinder.services.ICompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/company")
public class CompanyController {

    @Autowired
    ICompanyService companyService;

    @Autowired
    IAccountService accountService;

    @GetMapping("/getAllCompanies")
    public List<Company> getAllCompanies(){
        return companyService.getAllCompanies();
    }

    @GetMapping("/getCompanyByAccountId")
    public ResponseEntity<Company> getCompanyByAccountId(@RequestParam String accountId) {
        return ResponseEntity.ok(companyService.findCompanyByAccountIdOrThrow(accountId));
    }

    @GetMapping("/getCompaniesPaginated")
    public Page<Company> getCompaniesPaginated(@RequestParam Integer page, @RequestParam Integer size) {
        return companyService.getCompaniesPaginated(page, size);
    }

    @GetMapping("/getCompaniesByStatusVerifiedPaginated")
    public Page<Company> getCompaniesByStatusVerifiedPaginated(@RequestParam Integer page, Integer size) {
        return companyService.getCompaniesByStatusPaginated(AccountStatus.VERIFIED.name(), page, size);
    }

    @PostMapping("/addCompany")
    public ResponseEntity<String> addCompany(@RequestBody CompanyBean newCompany){
        Account account = accountService.findAccountByIdOrThrow(newCompany.getAccountId());
        if(!account.getAccountRole().equals(AccountRole.COMPANY)){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Ce compte n'est pas référencé comme un profil entreprise");
        }
        if(companyService.checkIfExistCompanyByAccountId(newCompany.getAccountId())){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Le profil entreprise de ce compte a déjà été créé");
        }
        Company company = new Company(account, newCompany.getName(), newCompany.getDescription(), "", "");
        companyService.addCompany(company);
        return ResponseEntity.ok("Nouveau profil entreprise enregistré avec succès");
    }

    @PatchMapping("/updateCompany")
    public ResponseEntity<String> updateCompany(@RequestBody CompanyBean updatedCompany) {
        Company company = companyService.findCompanyByIdOrThrow(updatedCompany.getCompanyId());
        company.setCompanyName(updatedCompany.getName());
        company.setCompanyDescription(updatedCompany.getDescription());
        companyService.updateCompany(company);
        return ResponseEntity.ok("Le profil de l'entreprise a été mis à jour avec succès");
    }
}
