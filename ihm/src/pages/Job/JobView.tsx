import { Box, Text } from "@chakra-ui/react";
import PageContainer from "../../layouts/PageContainer";
import JobCreateForm from "../../components/job/JobCreateForm";

function JobView() {
  return (
    <PageContainer>
      <Box
        w="xl"
        borderWidth="1px"
        borderRadius="lg"
        overflow="hidden"
        px={8}
        py={4}
        mb={4}
        mx={"auto"}
        className="flex flex-col items-center"
      >
        <Text fontSize="3xl" align="center" mb={3}>
          Nouvelle offre
        </Text>
        <JobCreateForm />
      </Box>
    </PageContainer>
  );
}

export default JobView;
