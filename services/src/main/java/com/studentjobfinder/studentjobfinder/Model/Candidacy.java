package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document("Candidacy")
@Getter
@Setter
public class Candidacy {
    @Id
    private String candidacyId;
    @DBRef
    private Student student;
    @DBRef
    private Job job;
    private String candidacyCoverLetter;
    private LocalDateTime candidacyCreationDate;
    private CandidacyStatus candidacyStatus;

    public Candidacy(Student student, Job job, String candidacyCoverLetter, LocalDateTime candidacyCreationDate, CandidacyStatus candidacyStatus){
        this.student = student;
        this.job = job;
        this.candidacyCoverLetter = candidacyCoverLetter;
        this.candidacyCreationDate = candidacyCreationDate;
        this.candidacyStatus = candidacyStatus;
    }

}
