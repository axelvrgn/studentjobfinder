import { NavLink } from "react-router-dom";
import { ROUTES } from "../../data/routes";
import Logo from "../Logo";

const routeList = ROUTES.map((route) => (
  <NavLink
    to={route.path}
    key={route.label}
    className={({ isActive }) =>
      "border-b-2 hover:border-purple-600 h-14 flex mx-2 " +
      (isActive ? "border-purple-600" : "border-transparent")
    }
  >
    <div className="px-2 self-center">{route.label}</div>
  </NavLink>
));

export function Navbar() {
  return (
    <nav className="border-b">
      <div className="navbar mx-auto flex items-center h-14">
        <NavLink to="/">
          <Logo />
        </NavLink>
        <div className="ml-auto text-purple-700 font-medium flex items-center">
          {routeList}
          <NavLink
            to={"/login"}
            key={"Connexion"}
            className={
              "bg-purple-700 hover:bg-purple-600 h-10 rounded-full flex mx-2 "
            }
          >
            <div className="px-4 text-white self-center">Connexion</div>
          </NavLink>
        </div>
      </div>
    </nav>
  );
}
