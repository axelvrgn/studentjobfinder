package com.studentjobfinder.studentjobfinder.Model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Token")
@Getter
@Setter
public class Token {
    @Id
    public String tokenId;
    public String token;
    public TokenType tokenType;
    public boolean tokenRevoked;
    public boolean tokenExpired;
    @DBRef
    public Account account;

    public Token(String token, boolean tokenRevoked, boolean tokenExpired, Account account){
        this.token = token;
        this.tokenRevoked = tokenRevoked;
        this.tokenExpired = tokenExpired;
        this.tokenType = TokenType.BEARER;
        this.account = account;
    }
}
