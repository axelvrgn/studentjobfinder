import axios from "axios";
import { getUserToken, userIsLogged } from "../utils/authentication";

const Axios = axios.create({
  baseURL: "http://localhost:8080/api/",
  withCredentials: false,
  headers: { "Access-Control-Allow-Origin": "*" },
});

Axios.interceptors.request.use((request) => {
  if (userIsLogged()) {
    request.headers.Authorization = "Bearer " + getUserToken();
  }
  return request;
});

export default Axios;
