import Axios from "./callerService";
import {
  JobBean,
  PageableJobBean,
} from "./interfaces-generated/STUDENTJOBFINDER-INTERFACES";

type paginatedQueryParams = {
  keyword?: string;
  location?: string;
  type?: string;
  page: number;
  size: number;
};

const addJob = async (job: JobBean) => {
  return Axios.post("/job/addJob", job);
};

const getAllJobs = async () => {
  return Axios.get<JobBean[]>("/job/getAll");
};

const getJobsPaginated = async ({
  keyword,
  location,
  type,
  page,
  size,
}: paginatedQueryParams) => {
  return Axios.get<PageableJobBean>("/job/getJobsPaginated", {
    params: {
      ...(keyword && { keyword }),
      ...(location && { location }),
      ...(type && { type }),
      page,
      size,
    },
  });
};

const getJobsByCompanyPaginated = async ({
  page,
  size,
}: paginatedQueryParams) => {
  return Axios.get<JobBean[]>("/job/getJobsByCompanyPaginated", {
    params: { page: page, size: size },
  });
};

const getJobsByStatusVerifiedPaginated = async ({
  page,
  size,
}: paginatedQueryParams) => {
  return Axios.get<JobBean[]>("/job/getJobsByStatusVerifiedPaginated", {
    params: { page: page, size: size },
  });
};

const updateJob = async (job: JobBean) => {
  return Axios.patch("/job/updateJob", job);
};

const updateJobStatusToVerified = async (jobId: string) => {
  return Axios.patch("/job/updateJobStatusToVerified", null, {
    params: { jobId },
  });
};

const updateJobStatusToClosed = async (jobId: string) => {
  return Axios.patch("/job/updateJobStatusToClosed", null, {
    params: { jobId },
  });
};

const deleteJobByStatusUnverified = async (jobId: string) => {
  return Axios.delete("/job/deleteJobByStatusUnverified", {
    params: { jobId },
  });
};

export const jobService = {
  addJob,
  getAllJobs,
  getJobsPaginated,
  getJobsByCompanyPaginated,
  getJobsByStatusVerifiedPaginated,
  updateJob,
  updateJobStatusToVerified,
  updateJobStatusToClosed,
  deleteJobByStatusUnverified,
};
