package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Job;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface JobRepository extends MongoRepository<Job,String> {

    Page<Job> findJobsByCompanyCompanyId(String companyId, Pageable pageable);
    Page<Job> findJobsByJobStatus(String jobStatus, Pageable pageable);

    Page<Job> findJobsByJobTitleIsLikeOrCompanyCompanyNameIsLike(String keyword, String keyword2, Pageable pageable);
}
