import { useState } from "react";
import { useToasts } from "../../../contexts/toast";
import { authenticateService } from "../../../services/authenticateService";
import { saveUserToken } from "../../../utils/authentication";
import { Button } from "@chakra-ui/react";
import { COMPANY, ERROR, STUDENT, SUCCESS } from "../../../data/constants";
import { companyService } from "../../../services/companyService";
import { studentService } from "../../../services/studentService";
import FormRegisterStep1 from "./RegisterStep1Form";
import FormRegisterCompany from "./Company/CompanyRegisterForm";
import FormRegisterStudent from "./Student/StudentRegisterForm";

function FormRegister() {
  const [step, setStep] = useState<number>(1);
  const [accountRole, setAccountRole] = useState<string>("");
  const [registerButtonIsLoading, setRegisterButtonIsLoading] =
    useState<boolean>(false);
  const { pushToast } = useToasts();

  const saveAccountRole = (role: string) => {
    setAccountRole(role);
    setNextStep();
  };

  const createCompany = (name: string, email: string, password: string) => {
    setRegisterButtonIsLoading(true);
    authenticateService
      .register({
        email,
        password,
        role: accountRole,
      })
      .then((res) => {
        setAccountRole(res.data.accountRole);
        saveUserToken(res.data.accessToken);
        saveCompany(res.data.accountId, name);
      })
      .catch((err) => {
        pushToast({ content: err.response.data, type: ERROR });
      })
      .finally(() => {
        setRegisterButtonIsLoading(false);
      });
  };

  const createStudent = (
    firstname: string,
    lastname: string,
    email: string,
    password: string
  ) => {
    setRegisterButtonIsLoading(true);
    authenticateService
      .register({
        email,
        password,
        role: accountRole,
      })
      .then((res) => {
        setAccountRole(res.data.accountRole);
        saveUserToken(res.data.accessToken);
        saveStudent(res.data.accountId, firstname, lastname);
      })
      .catch((err) => {
        pushToast({ content: err.response.data, type: ERROR });
      })
      .finally(() => {
        setRegisterButtonIsLoading(false);
      });
  };

  const saveCompany = (accountId: string, name: string) => {
    companyService
      .addCompany({ accountId, name })
      .then((res) => {
        pushToast({ content: res.data, type: SUCCESS });
      })
      .catch((err) => {
        pushToast({ content: err.response.data, type: ERROR });
      });
  };

  const saveStudent = (
    accountId: string,
    firstname: string,
    lastname: string
  ) => {
    studentService
      .addStudent({ accountId, firstname, lastname })
      .then((res) => {
        pushToast({ content: res.data, type: SUCCESS });
      })
      .catch((err) => {
        pushToast({ content: err.response.data, type: ERROR });
      });
  };

  const setNextStep = () => {
    setStep((prevState) => {
      return prevState + 1;
    });
  };

  const setPreviousStep = () => {
    setStep((prevState) => {
      return prevState - 1;
    });
  };

  return (
    <div className="flex flex-col w-80">
      {step === 1 && (
        <FormRegisterStep1
          accountRole={accountRole}
          saveAccountRole={saveAccountRole}
        />
      )}
      {step === 2 && accountRole === COMPANY && (
        <FormRegisterCompany
          createAccountCompany={createCompany}
          registerButtonIsLoading={registerButtonIsLoading}
        />
      )}
      {step === 2 && accountRole === STUDENT && (
        <FormRegisterStudent
          createAccountStudent={createStudent}
          registerButtonIsLoading={registerButtonIsLoading}
        />
      )}
      {step > 1 && (
        <Button
          type="button"
          variant="outline"
          colorScheme="purple"
          onClick={setPreviousStep}
          mt={3}
        >
          Revenir
        </Button>
      )}
    </div>
  );
}

export default FormRegister;
