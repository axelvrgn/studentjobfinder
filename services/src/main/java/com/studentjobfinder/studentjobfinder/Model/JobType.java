package com.studentjobfinder.studentjobfinder.Model;

public enum JobType {
    CDI, CDD, STAGE, ALTERNANCE, TEMPS_PARTIEL
}
