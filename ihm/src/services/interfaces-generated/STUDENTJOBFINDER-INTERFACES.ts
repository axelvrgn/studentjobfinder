/* eslint-disable */
/* tslint:disable */
/*
 * ---------------------------------------------------------------
 * ## THIS FILE WAS GENERATED VIA SWAGGER-TYPESCRIPT-API        ##
 * ##                                                           ##
 * ## AUTHOR: acacode                                           ##
 * ## SOURCE: https://github.com/acacode/swagger-typescript-api ##
 * ---------------------------------------------------------------
 */

export interface LoginBean {
  email: string;
  password: string;
}

export interface AuthenticationBean {
  accessToken: string;
  refreshToken: string;
  accountId: string;
  accountRole: string;
}

export interface JobBean {
  jobId?: string;
  companyId: string;
  companyName?: string;
  jobTitle: string;
  jobDescription?: string;
  jobCreationDate?: string;
  jobStartDate?: string;
  jobSalary?: string;
  jobType?: string;
  jobAdvantages?: string[];
  jobAddress?: AddressBean;
  jobStatus: string;
}

export interface AddressBean {
  addressId?: string;
  address?: string;
  addressCity?: string;
  addressPostalCode?: string;
}

export interface CompanyBean {
  companyId: string;
  account: AccountBean;
  companyName: string;
  companyDescription?: string;
  companySectorActivity?: string;
  companyWebSite?: string;
}

export interface AccountBean {
  accountId?: string;
  accountEmail?: string;
  accountPassword?: string;
  accountCreationDate?: string;
  accountRole?: "ADMIN" | "STUDENT" | "COMPANY";
  accountStatus?: "UNVERIFIED" | "VERIFIED" | "BLOCKED" | "BANNED";
}

export interface StudentBean {
  studentId?: string;
  account?: AccountBean;
  studentFirstname?: string;
  studentLastname?: string;
  studentDescription?: string;
  studentPhoneNumber?: string;
  studentPortfolio?: string;
  studentSkills?: string[];
}

export interface CandidacyBean {
  candidacyId?: string;
  student?: StudentBean;
  job?: JobBean;
  candidacyCoverLetter?: string;
  candidacyCreationDate?: string;
  candidacyStatus?: string;
}

export interface PageableJobBean {
  content: JobBean[];
  pageNumber: number;
  pageSize: number;
  numberOfElements: number;
  totalPages: number;
  totalElements: number;
}

export interface PageableCompanyBean {
  content: CompanyBean[];
  pageNumber: number;
  pageSize: number;
  numberOfElements: number;
  totalPages: number;
  totalElements: number;
}

export type QueryParamsType = Record<string | number, any>;
export type ResponseFormat = keyof Omit<Body, "body" | "bodyUsed">;

export interface FullRequestParams extends Omit<RequestInit, "body"> {
  /** set parameter to `true` for call `securityWorker` for this request */
  secure?: boolean;
  /** request path */
  path: string;
  /** content type of request body */
  type?: ContentType;
  /** query params */
  query?: QueryParamsType;
  /** format of response (i.e. response.json() -> format: "json") */
  format?: ResponseFormat;
  /** request body */
  body?: unknown;
  /** base url */
  baseUrl?: string;
  /** request cancellation token */
  cancelToken?: CancelToken;
}

export type RequestParams = Omit<FullRequestParams, "body" | "method" | "query" | "path">;

export interface ApiConfig<SecurityDataType = unknown> {
  baseUrl?: string;
  baseApiParams?: Omit<RequestParams, "baseUrl" | "cancelToken" | "signal">;
  securityWorker?: (securityData: SecurityDataType | null) => Promise<RequestParams | void> | RequestParams | void;
  customFetch?: typeof fetch;
}

export interface HttpResponse<D extends unknown, E extends unknown = unknown> extends Response {
  data: D;
  error: E;
}

type CancelToken = Symbol | string | number;

export enum ContentType {
  Json = "application/json",
  FormData = "multipart/form-data",
  UrlEncoded = "application/x-www-form-urlencoded",
  Text = "text/plain",
}

export class HttpClient<SecurityDataType = unknown> {
  public baseUrl: string = "/api";
  private securityData: SecurityDataType | null = null;
  private securityWorker?: ApiConfig<SecurityDataType>["securityWorker"];
  private abortControllers = new Map<CancelToken, AbortController>();
  private customFetch = (...fetchParams: Parameters<typeof fetch>) => fetch(...fetchParams);

  private baseApiParams: RequestParams = {
    credentials: "same-origin",
    headers: {},
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  constructor(apiConfig: ApiConfig<SecurityDataType> = {}) {
    Object.assign(this, apiConfig);
  }

  public setSecurityData = (data: SecurityDataType | null) => {
    this.securityData = data;
  };

  protected encodeQueryParam(key: string, value: any) {
    const encodedKey = encodeURIComponent(key);
    return `${encodedKey}=${encodeURIComponent(typeof value === "number" ? value : `${value}`)}`;
  }

  protected addQueryParam(query: QueryParamsType, key: string) {
    return this.encodeQueryParam(key, query[key]);
  }

  protected addArrayQueryParam(query: QueryParamsType, key: string) {
    const value = query[key];
    return value.map((v: any) => this.encodeQueryParam(key, v)).join("&");
  }

  protected toQueryString(rawQuery?: QueryParamsType): string {
    const query = rawQuery || {};
    const keys = Object.keys(query).filter((key) => "undefined" !== typeof query[key]);
    return keys
      .map((key) => (Array.isArray(query[key]) ? this.addArrayQueryParam(query, key) : this.addQueryParam(query, key)))
      .join("&");
  }

  protected addQueryParams(rawQuery?: QueryParamsType): string {
    const queryString = this.toQueryString(rawQuery);
    return queryString ? `?${queryString}` : "";
  }

  private contentFormatters: Record<ContentType, (input: any) => any> = {
    [ContentType.Json]: (input: any) =>
      input !== null && (typeof input === "object" || typeof input === "string") ? JSON.stringify(input) : input,
    [ContentType.Text]: (input: any) => (input !== null && typeof input !== "string" ? JSON.stringify(input) : input),
    [ContentType.FormData]: (input: any) =>
      Object.keys(input || {}).reduce((formData, key) => {
        const property = input[key];
        formData.append(
          key,
          property instanceof Blob
            ? property
            : typeof property === "object" && property !== null
            ? JSON.stringify(property)
            : `${property}`,
        );
        return formData;
      }, new FormData()),
    [ContentType.UrlEncoded]: (input: any) => this.toQueryString(input),
  };

  protected mergeRequestParams(params1: RequestParams, params2?: RequestParams): RequestParams {
    return {
      ...this.baseApiParams,
      ...params1,
      ...(params2 || {}),
      headers: {
        ...(this.baseApiParams.headers || {}),
        ...(params1.headers || {}),
        ...((params2 && params2.headers) || {}),
      },
    };
  }

  protected createAbortSignal = (cancelToken: CancelToken): AbortSignal | undefined => {
    if (this.abortControllers.has(cancelToken)) {
      const abortController = this.abortControllers.get(cancelToken);
      if (abortController) {
        return abortController.signal;
      }
      return void 0;
    }

    const abortController = new AbortController();
    this.abortControllers.set(cancelToken, abortController);
    return abortController.signal;
  };

  public abortRequest = (cancelToken: CancelToken) => {
    const abortController = this.abortControllers.get(cancelToken);

    if (abortController) {
      abortController.abort();
      this.abortControllers.delete(cancelToken);
    }
  };

  public request = async <T = any, E = any>({
    body,
    secure,
    path,
    type,
    query,
    format,
    baseUrl,
    cancelToken,
    ...params
  }: FullRequestParams): Promise<HttpResponse<T, E>> => {
    const secureParams =
      ((typeof secure === "boolean" ? secure : this.baseApiParams.secure) &&
        this.securityWorker &&
        (await this.securityWorker(this.securityData))) ||
      {};
    const requestParams = this.mergeRequestParams(params, secureParams);
    const queryString = query && this.toQueryString(query);
    const payloadFormatter = this.contentFormatters[type || ContentType.Json];
    const responseFormat = format || requestParams.format;

    return this.customFetch(`${baseUrl || this.baseUrl || ""}${path}${queryString ? `?${queryString}` : ""}`, {
      ...requestParams,
      headers: {
        ...(requestParams.headers || {}),
        ...(type && type !== ContentType.FormData ? { "Content-Type": type } : {}),
      },
      signal: (cancelToken ? this.createAbortSignal(cancelToken) : requestParams.signal) || null,
      body: typeof body === "undefined" || body === null ? null : payloadFormatter(body),
    }).then(async (response) => {
      const r = response as HttpResponse<T, E>;
      r.data = null as unknown as T;
      r.error = null as unknown as E;

      const data = !responseFormat
        ? r
        : await response[responseFormat]()
            .then((data) => {
              if (r.ok) {
                r.data = data;
              } else {
                r.error = data;
              }
              return r;
            })
            .catch((e) => {
              r.error = e;
              return r;
            });

      if (cancelToken) {
        this.abortControllers.delete(cancelToken);
      }

      if (!response.ok) throw data;
      return data;
    });
  };
}

/**
 * @title Swagger StudentJobFinder
 * @version 1.0.0
 * @baseUrl /api
 *
 * Student Job Finder
 */
export class Api<SecurityDataType extends unknown> extends HttpClient<SecurityDataType> {
  auth = {
    /**
     * @description permet à un utilisateur de s'authentifier
     *
     * @name Authenticate
     * @summary authenticate
     * @request POST:/auth/authenticate
     */
    authenticate: (body: LoginBean, params: RequestParams = {}) =>
      this.request<AuthenticationBean[], any>({
        path: `/auth/authenticate`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),
  };
  account = {
    /**
     * @description permet de modifier le statut d'un compte à vérifié
     *
     * @name UpdateAccountStatusToVerified
     * @summary update account status to verified
     * @request PATCH:/account/updateAccountStatusToVerified
     */
    updateAccountStatusToVerified: (params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/account/updateAccountStatusToVerified`,
        method: "PATCH",
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'un compte à banni
     *
     * @name UpdateAccountStatusToBanned
     * @summary update account status to banned
     * @request PATCH:/account/updateAccountStatusToBanned
     */
    updateAccountStatusToBanned: (params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/account/updateAccountStatusToBanned`,
        method: "PATCH",
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'un compte à bloqué
     *
     * @name UpdateAccountStatusToBlocked
     * @summary update account status to blocked
     * @request PATCH:/account/updateAccountStatusToBlocked
     */
    updateAccountStatusToBlocked: (params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/account/updateAccountStatusToBlocked`,
        method: "PATCH",
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le type d'un compte à admin
     *
     * @name UpdateAccountTypeToAdmin
     * @summary update account type to admin
     * @request PATCH:/account/updateAccountTypeToAdmin
     */
    updateAccountTypeToAdmin: (params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/account/updateAccountTypeToAdmin`,
        method: "PATCH",
        format: "json",
        ...params,
      }),
  };
  company = {
    /**
     * @description permet d'ajouter un profil entreprise
     *
     * @name AddCompany
     * @summary add company
     * @request POST:/company/addCompany
     */
    addCompany: (body: CompanyBean, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/company/addCompany`,
        method: "POST",
        body: body,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier un profil entreprise
     *
     * @name UpdateCompany
     * @summary update company
     * @request PATCH:/company/updateCompany
     */
    updateCompany: (body: CompanyBean, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/company/updateCompany`,
        method: "PATCH",
        body: body,
        format: "json",
        ...params,
      }),
  };
  job = {
    /**
     * @description permet d'ajouter une offre d'emplois
     *
     * @name AddJob
     * @summary add job
     * @request POST:/job/addJob
     */
    addJob: (body: JobBean, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/job/addJob`,
        method: "POST",
        body: body,
        format: "json",
        ...params,
      }),

    /**
     * @description retourne toutes les offres d'emplois
     *
     * @name GetAllJobs
     * @summary get all jobs
     * @request GET:/job/getAllJobs
     */
    getAllJobs: (params: RequestParams = {}) =>
      this.request<JobBean[], any>({
        path: `/job/getAllJobs`,
        method: "GET",
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier une offre d'emplois
     *
     * @name UpdateJob
     * @summary update job
     * @request PATCH:/job/updateJob
     */
    updateJob: (body: JobBean, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/job/updateJob`,
        method: "PATCH",
        body: body,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'une offre d'emplois à vérifié
     *
     * @name UpdateJobStatusToVerified
     * @summary update job status to verified
     * @request PATCH:/job/updateJobStatusToVerified
     */
    updateJobStatusToVerified: (
      query: {
        /** job id */
        jobId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/job/updateJobStatusToVerified`,
        method: "PATCH",
        query: query,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'une offre d'emplois à fermé
     *
     * @name UpdateJobStatusToClosed
     * @summary update job status to closed
     * @request PATCH:/job/updateJobStatusToClosed
     */
    updateJobStatusToClosed: (
      query: {
        /** job id */
        jobId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/job/updateJobStatusToClosed`,
        method: "PATCH",
        query: query,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de supprimer une offre d'emplois si son statut est non vérifié
     *
     * @name DeleteJobByStatusUnverified
     * @summary delete job by status unveri@fied
     * @request DELETE:/job/deleteJobByStatusUnverified
     */
    deleteJobByStatusUnverified: (
      query: {
        /** job id */
        jobId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/job/deleteJobByStatusUnverified`,
        method: "DELETE",
        query: query,
        format: "json",
        ...params,
      }),
  };
  candidacy = {
    /**
     * @description permet de candidater à une offre d'emplois
     *
     * @name AddCandidacy
     * @summary add candidacy
     * @request POST:/candidacy/addCandidacy
     */
    addCandidacy: (body: CandidacyBean, params: RequestParams = {}) =>
      this.request<string, any>({
        path: `/candidacy/addCandidacy`,
        method: "POST",
        body: body,
        type: ContentType.Json,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'une candidature à vue
     *
     * @name UpdateCandidacyStatusToViewed
     * @summary update candidacy status to viewed
     * @request PATCH:/candidacy/updateCandidacyStatusToViewed
     */
    updateCandidacyStatusToViewed: (
      query: {
        /** candidacy id */
        candidacyId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/candidacy/updateCandidacyStatusToViewed`,
        method: "PATCH",
        query: query,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'une candidature à accepté
     *
     * @name UpdateCandidacyStatusToAccepted
     * @summary update candidacy status to accepted
     * @request PATCH:/candidacy/updateCandidacyStatusToAccepted
     */
    updateCandidacyStatusToAccepted: (
      query: {
        /** candidacy id */
        candidacyId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/candidacy/updateCandidacyStatusToAccepted`,
        method: "PATCH",
        query: query,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de modifier le statut d'une candidature à refusé
     *
     * @name UpdateCandidacyStatusToRefused
     * @summary update candidacy status to refused
     * @request PATCH:/candidacy/updateCandidacyStatusToRefused
     */
    updateCandidacyStatusToRefused: (
      query: {
        /** candidacy id */
        candidacyId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/candidacy/updateCandidacyStatusToRefused`,
        method: "PATCH",
        query: query,
        format: "json",
        ...params,
      }),

    /**
     * @description permet de supprimer une candidature si son statut est non vue
     *
     * @name DeleteCandidacyByStatusUnviewed
     * @summary delete candidacy by status unviewed
     * @request DELETE:/candidacy/deleteCandidacyByStatusUnviewed
     */
    deleteCandidacyByStatusUnviewed: (
      query: {
        /** candidacy id */
        candidacyId: string;
      },
      params: RequestParams = {},
    ) =>
      this.request<string, any>({
        path: `/candidacy/deleteCandidacyByStatusUnviewed`,
        method: "DELETE",
        query: query,
        format: "json",
        ...params,
      }),
  };
}
