package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Address;
import com.studentjobfinder.studentjobfinder.Model.Company;
import com.studentjobfinder.studentjobfinder.Model.JobStatus;
import com.studentjobfinder.studentjobfinder.Model.Job;
import com.studentjobfinder.studentjobfinder.repository.CompanyRepository;
import com.studentjobfinder.studentjobfinder.repository.JobRepository;
import io.micrometer.common.util.StringUtils;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class JobService implements IJobService {

    @Autowired
    JobRepository jobRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public Job addJob(Job job) {
        return jobRepository.save(job);
    }

    @Override
    public Page<Job> getJobsByCompanyIdPaginated(String companyId, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return jobRepository.findJobsByCompanyCompanyId(companyId, paging);
    }

    @Override
    public Page<Job> getJobsPaginated(Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return jobRepository.findAll(paging);
    }

    @Override
    public Page<Job> getJobsByKeywordAndLocationAndTypePaginated(String keyword, String location, String type, Integer page, Integer size){
        Pageable paging = PageRequest.of(page, size);

        var jobQuery = new Query().with(paging).with(Sort.by(Sort.Direction.DESC, "jobCreationDate"));

        if(StringUtils.isNotBlank(keyword)) {
            var companyQuery = new Query().with(paging);
            companyQuery.addCriteria(Criteria.where("companyName").regex(keyword, "i"));
            List<Company> companies = mongoTemplate.find(companyQuery, Company.class);
            List<ObjectId> companyIds = new ArrayList<>();
            for (Company company : companies) {
                companyIds.add(new ObjectId(company.getCompanyId()));
            }

            jobQuery.addCriteria(new Criteria().orOperator(
                    Criteria.where("jobTitle").regex(keyword, "i"),
                    Criteria.where("company.$id").in(companyIds))
            );
        }

        if(StringUtils.isNotBlank(location)) {
            var addressQuery = new Query().with(paging);
            addressQuery.addCriteria(new Criteria().orOperator(
                    Criteria.where("addressAddress").regex(location, "i"),
                    Criteria.where("addressCity").regex(location, "i"),
                    Criteria.where("addressPostalCode").regex(location, "i")));
            List<Address> addresses = mongoTemplate.find(addressQuery, Address.class);
            List<ObjectId> addressIds = new ArrayList<>();
            for (Address address : addresses) {
                addressIds.add(new ObjectId(address.getAddressId()));
            }
            jobQuery.addCriteria(Criteria.where("address.$id").in(addressIds));
        }

        if(StringUtils.isNotBlank(type)) {
            jobQuery.addCriteria(Criteria.where("jobType").regex(type ,"i"));
        }

        return PageableExecutionUtils.getPage(
                mongoTemplate.find(jobQuery, Job.class),
                paging,
                () -> mongoTemplate.count(jobQuery, Job.class)
        );
    }

    public Page<Job> getJobsByStatusPaginated(String jobStatus, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return jobRepository.findJobsByJobStatus(jobStatus, paging);
    }

    @Override
    public List<Job> getAllJobs() {
        return jobRepository.findAll();
    }

    @Override
    public Job updateJobStatutFromJobId(String jobId, JobStatus jobStatus) {
        Job job = findJobByIdOrThrow(jobId);
        job.setJobStatus(jobStatus);
        return jobRepository.save(job);
    }

    @Override
    public Job updateJob(Job jobUpdated) {
        return jobRepository.save(jobUpdated);
    }

    @Override
    public void deleteJobById(String jobId) {
        jobRepository.deleteById(jobId);
    }

    @Override
    public Job findJobByIdOrThrow(String jobId) throws ResourceNotFoundException {
        return jobRepository.findById(jobId).orElseThrow(() -> new ResourceNotFoundException("Offre d'emplois introuvable"));
    }
}
