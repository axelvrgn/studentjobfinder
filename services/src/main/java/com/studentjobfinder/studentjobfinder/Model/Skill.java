package com.studentjobfinder.studentjobfinder.Model;

import org.springframework.data.annotation.Id;
import org.springframework.lang.Nullable;

public class Skill {
    @Id
    private String skillId;
    private String skillName;
    private String skillLevel;
    @Nullable
    private String skillComment;

    public Skill(String skillName, String skillLevel, String skillComment) {
        this.skillName = skillName;
        this.skillLevel = skillLevel;
        this.skillComment = skillComment;
    }
}
