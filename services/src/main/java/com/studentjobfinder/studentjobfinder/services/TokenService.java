package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Token;
import com.studentjobfinder.studentjobfinder.repository.TokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TokenService {
    @Autowired
    TokenRepository tokenRepository;


    public Token findTokenByAccountIdOrThrow(String accountId) throws ResourceNotFoundException {

        Optional<Token> optToken = tokenRepository.findByAccount_AccountId(accountId);
        if(optToken.isPresent()){
            return optToken.get();
        }

        return null;
    }
}
