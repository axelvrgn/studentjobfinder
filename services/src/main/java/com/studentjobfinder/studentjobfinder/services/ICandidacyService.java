package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Candidacy;
import com.studentjobfinder.studentjobfinder.Model.CandidacyStatus;
import org.springframework.data.domain.Page;

public interface ICandidacyService {
    Candidacy getCandidacyById(String candidacyId);
    Page<Candidacy> getCandidaciesByStudentIdPaginated(String studentId, Integer page, Integer size);
    Page<Candidacy> getCandidaciesByJobIdPaginated(String jobId, Integer page, Integer size);
    Candidacy addCandidacy(Candidacy candidacy);
    Candidacy updateCandidacyStatut (String candidacyId, CandidacyStatus candidacyStatus);
    void deleteCandidacyUnviewed(String candidacyId);
    boolean checkIfExistCandidacyByJobIdAndStudentId (String jobId, String studentId);
    Candidacy findCandidacyByIdOrThrow (String candidacyId);
}
