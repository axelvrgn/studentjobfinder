package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Account;
import com.studentjobfinder.studentjobfinder.Model.AccountRole;
import com.studentjobfinder.studentjobfinder.Model.AccountStatus;

public interface IAccountService {
    Account addAccount(Account account);
    Account updateAccountStatut(String accountId, AccountStatus accountStatus);
    Account updateAccountRole(String accountId, AccountRole accountRole);
    Account findAccountByIdOrThrow(String accountId);
    Boolean existAccountByEmail(String email);
}
