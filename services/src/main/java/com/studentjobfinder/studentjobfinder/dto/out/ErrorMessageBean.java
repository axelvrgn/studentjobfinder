package com.studentjobfinder.studentjobfinder.dto.out;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class ErrorMessageBean {
    private int statusCode;
    private Date timestamp;
    private String message;
    private String description;

    public ErrorMessageBean(int statusCode, Date timestamp, String message, String description) {
        this.statusCode = statusCode;
        this.timestamp = timestamp;
        this.message = message;
        this.description = description;
    }
}
