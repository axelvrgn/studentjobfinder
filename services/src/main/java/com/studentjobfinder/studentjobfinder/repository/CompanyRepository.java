package com.studentjobfinder.studentjobfinder.repository;

import com.studentjobfinder.studentjobfinder.Model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CompanyRepository extends MongoRepository<Company,String> {

    Page<Company> findCompaniesByAccount_AccountStatus(String accountStatus, Pageable pageable);

    Optional<Company> findCompanyByAccount_AccountId(String accountId);

    boolean existsByAccount_AccountId(String accountId);
}
