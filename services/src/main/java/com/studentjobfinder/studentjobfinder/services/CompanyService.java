package com.studentjobfinder.studentjobfinder.services;

import com.studentjobfinder.studentjobfinder.Model.Company;
import com.studentjobfinder.studentjobfinder.repository.AccountRepository;
import com.studentjobfinder.studentjobfinder.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService implements ICompanyService {

    @Autowired
    AccountService accountService;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    AccountRepository accountRepository;

    @Override
    public List<Company> getAllCompanies() {
        return companyRepository.findAll();
    }

    @Override
    public Page<Company> getCompaniesPaginated(Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return companyRepository.findAll(paging);
    }

    @Override
    public Page<Company> getCompaniesByStatusPaginated(String accountStatus, Integer page, Integer size) {
        Pageable paging = PageRequest.of(page, size);
        return companyRepository.findCompaniesByAccount_AccountStatus(accountStatus, paging);
    }

    @Override
    public Company findCompanyByAccountIdOrThrow(String accountId) {
         return companyRepository.findCompanyByAccount_AccountId(accountId).orElseThrow(()-> new ResourceNotFoundException("Aucune entreprise n'appartient à ce compte"));
    }

    @Override
    public Company findCompanyByIdOrThrow (String companyId) throws ResourceNotFoundException {
        return companyRepository.findById(companyId).orElseThrow(() -> new ResourceNotFoundException("Candidature introuvable"));
    }

    @Override
    public Company addCompany(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public Company updateCompany(Company company) {
        return companyRepository.save(company);
    }

    @Override
    public boolean checkIfExistCompanyByAccountId (String accountId) {
        return companyRepository.existsByAccount_AccountId(accountId);
    }
}
