import { Button, FormControl, FormLabel, Radio, Stack } from "@chakra-ui/react";
import { Controller, useForm } from "react-hook-form";
import { COMPANY, STUDENT } from "../../../data/constants";

type FormRegisterStep1Props = {
  accountRole: string;
  saveAccountRole: (role: string) => void;
};

interface IFormRegisterStep1 {
  role: string;
}

function FormRegisterStep1({
  accountRole,
  saveAccountRole,
}: FormRegisterStep1Props) {
  const {
    control,
    handleSubmit,
    formState: { isValid },
  } = useForm<IFormRegisterStep1>({
    defaultValues: { role: accountRole },
  });

  const saveRole = (formRegisterStep1Value: IFormRegisterStep1) => {
    saveAccountRole(formRegisterStep1Value.role);
  };

  return (
    <form className="w-96 flex flex-col" onSubmit={handleSubmit(saveRole)}>
      <FormControl mb={3}>
        <FormLabel htmlFor="role">Je suis</FormLabel>
        <Stack>
          <Controller
            control={control}
            name="role"
            render={({ field: { onChange, value } }) => (
              <Radio
                id="role"
                value={STUDENT}
                onChange={onChange}
                isChecked={value === STUDENT}
                colorScheme="purple"
              >
                un étudiant
              </Radio>
            )}
          />
          <Controller
            control={control}
            name="role"
            render={({ field: { onChange, value } }) => (
              <Radio
                id="role"
                value={COMPANY}
                onChange={onChange}
                isChecked={value === COMPANY}
                colorScheme="purple"
              >
                une entreprise
              </Radio>
            )}
          />
        </Stack>
      </FormControl>
      <Button type="submit" colorScheme="purple" mt={3} isDisabled={!isValid}>
        Suivant
      </Button>
    </form>
  );
}

export default FormRegisterStep1;
