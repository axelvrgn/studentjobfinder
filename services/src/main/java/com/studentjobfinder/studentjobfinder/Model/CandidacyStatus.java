package com.studentjobfinder.studentjobfinder.Model;

public enum CandidacyStatus {
    UNVIEWED, VIEWED, ACCEPTED, REFUSED
}
